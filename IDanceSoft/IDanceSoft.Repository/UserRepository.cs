﻿using System.Threading.Tasks;
using IDanceSoft.Repository;
using IDanceSoft.Repository.Public;
using IDanceSoft.Repository.Public.Model;
using Xamarin.Forms;

[assembly: Dependency(typeof(UserRepository))]
namespace IDanceSoft.Repository
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public Task<User> GetUser()
        {
            return Database.Table<User>().FirstOrDefaultAsync();
        }
    }
}
