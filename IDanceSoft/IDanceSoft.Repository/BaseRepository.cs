﻿using System.Threading.Tasks;
using IDanceSoft.Repository.Public;
using IDanceSoft.Repository.Public.Model;
using SQLite;

namespace IDanceSoft.Repository
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : Entity, new()
    {
        protected SQLiteAsyncConnection Database 
        {
            get { return AppDatabase.Instance.Database; }
        }

        public Task<int> Delete(T entity)
        {
            return Database.DeleteAsync(entity);
        }

        public Task<T> Get(int id)
        {
            return Database.Table<T>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public Task<int> Save(T entity)
        {
            if (entity.Id != 0) 
            {
                return Database.UpdateAsync(entity);
            }
            else 
            {
                return Database.InsertAsync(entity);
            }
        }
    }
}
