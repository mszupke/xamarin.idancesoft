﻿using System;
using IDanceSoft.Controlls;
using IDanceSoft.iOS.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(BorderBottomFrame), typeof(BorderBottomFrameRenderer))]
namespace IDanceSoft.iOS.Renderers
{
	public class BorderBottomFrameRenderer : VisualElementRenderer<StackLayout>
    {
        public BorderBottomFrameRenderer()
        {
			Layer.BorderColor = UIColor.Blue.CGColor;
            Layer.BorderWidth = 1;
        }
    }
}
