﻿using System;
using System.IO;
using Xamarin.Forms;
using IDanceSoft.Services;
using IDanceSoft.Droid.Services;

[assembly: Dependency(typeof(FileService))]
namespace IDanceSoft.Droid.Services
{
    public class FileService : IFileService
    {
        public string GetLocalFilePath(string filename)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return Path.Combine(path, filename);
        }
    }
}