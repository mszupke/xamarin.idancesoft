﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;

namespace IDanceSoft.Droid.Services
{
	[Service]
	public class SearchClientService : BaseTaskService
    {
		private string _searchText;

		[return: GeneratedEnum]
		public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
		{
			if (intent != null && intent.Extras != null) 
			{
				_searchText = intent.Extras.GetString("SearchText");
			}

			return base.OnStartCommand(intent, flags, startId);
		}

		protected override void HandleTaskService()
		{
			ServiceManager.HandleSearchClient(_searchText);
		}
	}
}
