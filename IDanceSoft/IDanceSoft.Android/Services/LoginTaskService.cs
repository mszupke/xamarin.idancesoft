﻿using Android.App;
using Android.Content;
using Android.Runtime;

namespace IDanceSoft.Droid.Services
{
    [Service]
    public class LoginTaskService : BaseTaskService
    {
        private string _username;
        private string _password;

        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            if (intent != null && intent.Extras != null)
            {
                _username = intent.Extras.GetString(Constants.Username);
                _password = intent.Extras.GetString(Constants.Password);
            }

            return base.OnStartCommand(intent, flags, startId);
        }

        protected override void HandleTaskService()
        {
            ServiceManager.HandleLogin(_username, _password);
        }
    }
}