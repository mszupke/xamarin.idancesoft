﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;

namespace IDanceSoft.Droid.Services
{
	[Service]
	public class SaveClientService : BaseTaskService
    {
		private string _id;
		private string _name;
		private string _nameApp;
		private string _phone;
		private string _email;
		private string _cardNumber;
		private string _birthDate;
		private string _sex;
		private string _course;
		private string _partner;
		private string _confirm;

		[return: GeneratedEnum]
		public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
		{
			if (intent != null && intent.Extras != null)
			{
				_id = intent.Extras.GetString(Constants.Id);
				_name = intent.Extras.GetString(Constants.Name);
				_nameApp = intent.Extras.GetString(Constants.NameApp);
				_phone = intent.Extras.GetString(Constants.Phone);
				_email = intent.Extras.GetString(Constants.Email);
				_cardNumber = intent.Extras.GetString(Constants.CardNumber);
				_birthDate = intent.Extras.GetString(Constants.BirthDate);
				_sex = intent.Extras.GetString(Constants.Sex);
				_course = intent.Extras.GetString(Constants.Course);
				_partner = intent.Extras.GetString(Constants.Partner);
				_confirm = intent.Extras.GetString(Constants.Confirm);
			}

			return base.OnStartCommand(intent, flags, startId);
		}

		protected override void HandleTaskService()
		{
			ServiceManager.HandleSaveClient(_id, _name, _nameApp, _phone, _email, _cardNumber, _birthDate, _sex, _course, _partner, _confirm);
		}
	}
}
