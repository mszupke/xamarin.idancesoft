﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using IDanceSoft.Messages;
using IDanceSoft.Services;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace IDanceSoft.Droid.Services
{
    public abstract class BaseTaskService : Service
    {
        private CancellationTokenSource _cts;
        protected IServiceManager ServiceManager { get; private set; }

        public BaseTaskService()
        {
            ServiceManager = DependencyService.Get<IServiceManager>();
        }

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            _cts = new CancellationTokenSource();

            Task.Run(() =>
            {
                try
                {
                    HandleTaskService();
                }
                catch (System.OperationCanceledException)
                {

                }
                finally
                {
                    if (_cts.IsCancellationRequested)
                    {
                        var message = new CancelledMessage();
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MessagingCenter.Send(message, CancelledMessage.Tag);
                        });
                    }
                }
            }, _cts.Token);

            return StartCommandResult.Sticky;
        }

        protected abstract void HandleTaskService();
    }
}