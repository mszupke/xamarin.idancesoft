﻿using Android.App;
using Android.Content;
using Android.Runtime;

namespace IDanceSoft.Droid.Services
{
    [Service]
    public class GetClientListForCourseService : BaseTaskService
    {
        private string _courseId;

        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            if (intent != null && intent.Extras != null)
            {
                _courseId = intent.Extras.GetString(Constants.CourseId);
            }

            return base.OnStartCommand(intent, flags, startId);
        }

        protected override void HandleTaskService()
        {
            ServiceManager.HandleGetClientListForCourse(_courseId);
        }
    }
}
