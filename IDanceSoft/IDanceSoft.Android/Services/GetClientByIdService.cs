﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;

namespace IDanceSoft.Droid.Services
{
	[Service]
	public class GetClientByIdService : BaseTaskService
    {
		private string _id;

		[return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            if (intent != null && intent.Extras != null)
            {
                _id = intent.Extras.GetString("Id");
            }

            return base.OnStartCommand(intent, flags, startId);
        }

        protected override void HandleTaskService()
        {
			ServiceManager.HandleGetClientById(_id);
        }
    }
}
