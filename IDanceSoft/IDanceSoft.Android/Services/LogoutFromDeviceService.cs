﻿using Android.App;

namespace IDanceSoft.Droid.Services
{
    [Service]
    public class LogoutFromDeviceService : BaseTaskService
    {
        protected override void HandleTaskService()
        {
            ServiceManager.HandleLogoutFromDevice();
        }
    }
}
