﻿using System.Threading;
using Android.App;

namespace IDanceSoft.Droid.Services
{
    [Service]
    public class ReadUserFromDatabaseTaskService : BaseTaskService
    {
        protected override void HandleTaskService()
        {
            Thread.Sleep(1000);
            ServiceManager.HandleReadUserFromDatabaseTask();
        }
    }
}