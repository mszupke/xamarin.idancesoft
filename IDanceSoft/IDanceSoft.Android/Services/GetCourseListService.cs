﻿using Android.App;
using Android.Content;
using Android.Runtime;

namespace IDanceSoft.Droid.Services
{
    [Service]
    public class GetCourseListService : BaseTaskService
    {
        private string _schoolId;

        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            if (intent != null && intent.Extras != null)
            {
                _schoolId = intent.Extras.GetString(Constants.SchoolId);
            }

            return base.OnStartCommand(intent, flags, startId);
        }

        protected override void HandleTaskService()
        {
            ServiceManager.HandleGetCourseList(_schoolId);
        }
    }
}
