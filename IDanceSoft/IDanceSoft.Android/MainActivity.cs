﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Xamarin.Forms;
using IDanceSoft.Messages;
using Android.Content;
using IDanceSoft.Droid.Services;
using Acr.UserDialogs;

namespace IDanceSoft.Droid
{
    [Activity(Label = "IDanceSoft", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);

            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            LoadApplication(new App());

            UserDialogs.Init(() => (Activity)Forms.Context);

            WireUpReadUserFromDatabaseTask();
        }

        private void WireUpReadUserFromDatabaseTask()
        {
            MessagingCenter.Subscribe<StartReadUserFromDatabaseTaskMessage>(this, StartReadUserFromDatabaseTaskMessage.Tag, message => 
            {
                var intent = new Intent(this, typeof(ReadUserFromDatabaseTaskService));
                StartService(intent);
            });

            MessagingCenter.Subscribe<StartLoginMessage>(this, StartLoginMessage.Tag, message =>
            {
                var intent = new Intent(this, typeof(LoginTaskService));

                var bundle = new Bundle();
                bundle.PutString(Constants.Username, message.Username);
                bundle.PutString(Constants.Password, message.Password);

                intent.PutExtras(bundle);

                StartService(intent);
            });

			MessagingCenter.Subscribe<StartSearchClientMessage>(this, StartSearchClientMessage.Tag, message =>
            {
				var intent = new Intent(this, typeof(SearchClientService));

				var bundle = new Bundle();
				bundle.PutString("SearchText", message.SearchText);
				intent.PutExtras(bundle);   

                StartService(intent);
            });

			MessagingCenter.Subscribe<StartGetClientByIdMessage>(this, StartGetClientByIdMessage.Tag, message =>
            {
				var intent = new Intent(this, typeof(GetClientByIdService));

                var bundle = new Bundle();
                bundle.PutString(Constants.Id, message.Id);
                intent.PutExtras(bundle);

                StartService(intent);
            });

			MessagingCenter.Subscribe<StartSaveClientMessage>(this, StartSaveClientMessage.Tag, message =>
			{
				var intent = new Intent(this, typeof(SaveClientService));

				var bundle = new Bundle();
				bundle.PutString(Constants.Id, message.Id);
				bundle.PutString(Constants.Name, message.Name);
				bundle.PutString(Constants.NameApp, message.Nameapp);
				bundle.PutString(Constants.Phone, message.Phone);
				bundle.PutString(Constants.Email, message.Email);
				bundle.PutString(Constants.CardNumber, message.CardNumber);
				bundle.PutString(Constants.BirthDate, message.BirthDate);
				bundle.PutString(Constants.Sex, message.Sex);
				bundle.PutString(Constants.Course, message.Course);
				bundle.PutString(Constants.Partner, message.Partner);
				bundle.PutString(Constants.Confirm, message.Confirm);
				intent.PutExtras(bundle);

				StartService(intent);
			});

            MessagingCenter.Subscribe<StartGetCourseListMessage>(this, StartGetCourseListMessage.Tag, message =>
            {
                var intent = new Intent(this, typeof(GetCourseListService));

                var bundle = new Bundle();
                bundle.PutString(Constants.SchoolId, message.SchoolId);
                intent.PutExtras(bundle);

                StartService(intent);
            });

            MessagingCenter.Subscribe<StartGetClientListForCourseMessage>(this, StartGetClientListForCourseMessage.Tag, message =>
            {
                var intent = new Intent(this, typeof(GetClientListForCourseService));

                var bundle = new Bundle();
                bundle.PutString(Constants.CourseId, message.CourseId);
                intent.PutExtras(bundle);

                StartService(intent);
            });

            MessagingCenter.Subscribe<StartLogoutFromDeviceMessage>(this, StartLogoutFromDeviceMessage.Tag, message =>
            {
                var intent = new Intent(this, typeof(LogoutFromDeviceService));
                StartService(intent);
            });
        }
    }
}

