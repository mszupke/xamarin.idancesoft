﻿using Android.Content;
using IDanceSoft.Controlls;
using IDanceSoft.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(NativeListView), typeof(NativeAndroidListViewRenderer))]
namespace IDanceSoft.Droid.Renderers
{
    public class NativeAndroidListViewRenderer : ListViewRenderer
    {
        public NativeAndroidListViewRenderer(Context context) : base(context)
        {
            
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ListView> e)
        {
            base.OnElementChanged(e);

            if (Control != null) 
            {  
                Control.VerticalScrollBarEnabled = false;  
            }  
        }
    }
}
