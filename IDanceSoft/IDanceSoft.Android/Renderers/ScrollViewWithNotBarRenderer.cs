﻿using Android.Content;
using IDanceSoft.Droid.Renderers;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using IDanceSoft.Controlls;

[assembly: ExportRenderer(typeof(ScrollViewWithNotBar), typeof(ScrollViewWithNotBarRenderer))]
namespace IDanceSoft.Droid.Renderers
{
    public class ScrollViewWithNotBarRenderer : ScrollViewRenderer
    {
        public ScrollViewWithNotBarRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || this.Element == null)
            {
                return;
            }

            if (e.OldElement != null)
            {
                e.OldElement.PropertyChanged -= OnElementPropertyChanged;
            }

            e.NewElement.PropertyChanged += OnElementPropertyChanged;

        }

        protected void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            HorizontalScrollBarEnabled = false;
            VerticalScrollBarEnabled = false;
        }
    }
}