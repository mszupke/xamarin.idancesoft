﻿using System;
using Android.Content;
using IDanceSoft.Controlls;
using IDanceSoft.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(TopRoundedButton), typeof(TopRoundedButtonRenderer))]
namespace IDanceSoft.Droid.Renderers
{
    public class TopRoundedButtonRenderer : SwitchRenderer
    {
        public TopRoundedButtonRenderer(Context context)
            : base(context)
        {
            
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Switch> e)
        {
            base.OnElementChanged(e);

            if (Control != null) 
            {
                Control.SetBackgroundResource(Resource.Drawable.blue_top_rounded_button_background);
            }
        }
    }
}
