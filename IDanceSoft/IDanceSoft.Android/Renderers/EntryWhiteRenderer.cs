﻿using Android.Content;
using Android.Graphics.Drawables;
using IDanceSoft.Controlls;
using IDanceSoft.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(EntryWhite), typeof(EntryWhiteRenderer))]
namespace IDanceSoft.Droid.Renderers
{
    public class EntryWhiteRenderer : EntryRenderer
    {
        public EntryWhiteRenderer(Context context)
            : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                var gradientDrawable = new GradientDrawable();
                gradientDrawable.SetColor(global::Android.Graphics.Color.White);
                gradientDrawable.SetCornerRadius(3);

                Control.Background = gradientDrawable;
            }
        }
    }
}