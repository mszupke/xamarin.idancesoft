﻿using Android.Content;
using Android.Support.V4.Content;
using IDanceSoft.Controlls;
using IDanceSoft.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(BorderBottomFrame), typeof(BorderBottomFrameRenderer))]
namespace IDanceSoft.Droid.Renderers
{
	public class BorderBottomFrameRenderer : VisualElementRenderer<StackLayout>
    {
		public BorderBottomFrameRenderer(Context context) : base(context)
        {
			Background = ContextCompat.GetDrawable(context, Resource.Drawable.blue_rect);
        }
    }
}
