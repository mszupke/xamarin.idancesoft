﻿using IDanceSoft.Models;

namespace IDanceSoft.Messages
{
    public class StartGetClientListForCourseMessage
    {
        public const string Tag = "StartGetClientListForCourse";

        public string CourseId { get; set; }
    }
}
