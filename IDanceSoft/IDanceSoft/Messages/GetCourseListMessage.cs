﻿using System.Collections.Generic;
using IDanceSoft.Models;

namespace IDanceSoft.Messages
{
    public class GetCourseListMessage : BaseMessage
    {
        public const string Tag = "GetCourseList";

        public IList<Course> Courses { get; set; }
    }
}
