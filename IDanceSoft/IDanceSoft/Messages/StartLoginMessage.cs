﻿namespace IDanceSoft.Messages
{
    public class StartLoginMessage
    {
        public const string Tag = "StartLoginMessage";

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
