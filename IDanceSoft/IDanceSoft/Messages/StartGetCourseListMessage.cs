﻿using System;
namespace IDanceSoft.Messages
{
    public class StartGetCourseListMessage
    {
        public const string Tag = "StartGetCourseList";

        public string SchoolId { get; set; }
    }
}
