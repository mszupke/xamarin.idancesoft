﻿namespace IDanceSoft.Messages
{
    public abstract class TransitionMessage
    {
        public TransitionType TransitionType { get; set; }
    }
}
