﻿namespace IDanceSoft.Messages
{
    public class ReadUserFromDatabaseTaskMessage
    {
        public const string Tag = "ReadUserFromDatabaseTaskMessage";

        public bool IsUserExist { get; set; }
    }
}
