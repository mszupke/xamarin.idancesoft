﻿using System;
using IDanceSoft.Models;

namespace IDanceSoft.Messages
{
	public class GetClientByIdMessage : BaseMessage
    {
		public const string Tag = "GetClientById";

		public Client Client { get; set; }
    }
}
