﻿using System;
namespace IDanceSoft.Messages
{
    public class StartSaveClientMessage
    {
		public const string Tag = "StartSaveClient";

        /***
         * ID klienta
         */
		public string Id { get; set; }

        /***
         * Imię i nazwisko klienta
         */
		public string Name { get; set; }

        /***
         * Imię i nazwisko osoby towarzyszącej
         */
		public string Nameapp { get; set; }

        /***
         * Numer telefonu
         */
		public string Phone { get; set; }

        /***
         * Adres E-mail
         */
		public string Email { get; set; }

        /***
         * Nr barcode lub QR
         */
		public string CardNumber { get; set; }

        /***
         * Data urodzenia yyyy-mm-dd
         */
		public string BirthDate { get; set; }

        /***
         * Płeć: 1-kobieta, 2-mężczyzna
         */
		public string Sex { get; set; }

        /***
         * Kurs klienta - ten parametr działa tylko wtedy, gdy zapisujemy nowego klienta
         */
		public string Course { get; set; }

        /***
         * ID szkoły
         */
		public string Partner { get; set; }

        /***
         * Potwierdzenie
         * 1 - będzie wysłane potwierdzenie do klienta z systemu
         * 0 - nie nie będzie wysłane
         */
		public string Confirm { get; set; }
    }
}
