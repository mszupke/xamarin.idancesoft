﻿using System;
using System.Collections.Generic;
using IDanceSoft.Models;

namespace IDanceSoft.Messages
{
	public class SearchClientMessage : BaseMessage
    {
		public const string Tag = "SearchClient";

        public List<ClientListItem> Clients { get; set; }
    }
}
