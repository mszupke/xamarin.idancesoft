﻿using System;
namespace IDanceSoft.Messages
{
    public class StartGetClientByIdMessage
    {
		public const string Tag = "StartGetClientById";

        public string Id { get; set; }
    }
}
