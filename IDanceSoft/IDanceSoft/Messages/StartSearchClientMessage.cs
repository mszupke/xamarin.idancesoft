﻿using System;
namespace IDanceSoft.Messages
{
    public class StartSearchClientMessage
    {
		public const string Tag = "StartSearchClient";

		public string SearchText { get; set; }
    }
}
