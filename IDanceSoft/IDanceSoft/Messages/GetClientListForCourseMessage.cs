﻿using System;
using System.Collections.Generic;
using IDanceSoft.Models;

namespace IDanceSoft.Messages
{
    public class GetClientListForCourseMessage : BaseMessage
    {
        public const string Tag = "GetClientListForCourse";

        public List<ClientListItem> Clients { get; set; }
    }
}
