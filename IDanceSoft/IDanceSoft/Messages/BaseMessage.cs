﻿namespace IDanceSoft.Messages
{
    public class BaseMessage
    {
        public int ErrorCode { get; set; }

        public string Message { get; set; }
    }
}
