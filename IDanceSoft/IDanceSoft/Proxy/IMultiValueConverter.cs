﻿using System;
using System.Globalization;

namespace IDanceSoft.Proxy
{
    public interface IMultiValueConverter
    {
        object Convert(object[] values, Type targetType, object parameter, CultureInfo culture);
    }
}
