﻿using IDanceSoft.Controlls;
using IDanceSoft.Services;
using IDanceSoft.Views;
using Xamarin.Forms;

namespace IDanceSoft
{
    public partial class App : Application
	{
        //private static DatabaseManager _databaseManager;
        //public static DatabaseManager DatabaseManager
        //{
        //    get
        //    {
        //        if (_databaseManager == null)
        //        {
        //            _databaseManager = new DatabaseManager(DependencyService.Get<IFileService>().GetLocalFilePath(AppSettings.DatabaseName));
        //        }
        //        return _databaseManager;
        //    }
        //}

        public App ()
		{
			InitializeComponent();

			MainPage = new TransitionNavigationPage(new SplashPage());
		}

		protected override void OnStart ()
		{
            // AppCenter.Start("android=8f6d1ace-7fb0-4a01-8604-214534a5771b;", typeof(Analytics), typeof(Crashes));
        }

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
