﻿using System.Linq;
using System.Threading.Tasks;
using IDanceSoft.API.Public.Model;
using IDanceSoft.Messages;
using IDanceSoft.Models;
using IDanceSoft.Repository.Public.Model;
using Xamarin.Forms;

namespace IDanceSoft.Services
{
    public class SearchClientServiceHandler : ServiceHandler<SearchClientResult>
    {
        private readonly string _searchText;

        public SearchClientServiceHandler(string searchText)
        {
            _searchText = searchText;
        }

        protected override SearchClientResult GetRestServiceResult(User user)
        {
            var task = Task.Run(async () => await RestService.SearchClient(_searchText, user.Login, user.Partner, user.SessionId));
            var result = task.Result;

            return result;
        }

        protected override void SendErrorMessage(int errorCode, string errorMessage)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                MessagingCenter.Send(new SearchClientMessage
                {
                    ErrorCode = errorCode,
                    Message = errorMessage
                }, SearchClientMessage.Tag);
            });
        }

        protected override void SendResultMessage(SearchClientResult result, int errorCode, string errorMessage)
        {
            int index = 1;
            var clients = result.Clients.Select(x => new ClientListItem
            {
                Id = x.Id,
                Name = x.Name,
                Email = x.Mail,
                Phone = x.Phone,
                Index = index++
            }).ToList();

            Device.BeginInvokeOnMainThread(() =>
            {
                MessagingCenter.Send(new SearchClientMessage
                {
                    Clients = clients,
                    ErrorCode = errorCode,
                    Message = errorMessage
                }, SearchClientMessage.Tag);
            });
        }
    }
}
