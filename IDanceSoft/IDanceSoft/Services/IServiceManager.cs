﻿namespace IDanceSoft.Services
{
    public interface IServiceManager
    {
        void HandleReadUserFromDatabaseTask();

        void HandleLogin(string username, string password);

		void HandleSearchClient(string searchText);

		void HandleGetClientById(string id);

		void HandleSaveClient(string id, string name, string nameApp, string phone, string email, string cardNumber, string birthDate, string sex, string course, string partner, string confirm);

        void HandleGetCourseList(string schoolId);

        void HandleGetClientListForCourse(string courseId);

        void HandleLogoutFromDevice();
    }
}
