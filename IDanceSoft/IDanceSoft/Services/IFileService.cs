﻿namespace IDanceSoft.Services
{
    public interface IFileService
    {
        string GetLocalFilePath(string filename);
    }
}
