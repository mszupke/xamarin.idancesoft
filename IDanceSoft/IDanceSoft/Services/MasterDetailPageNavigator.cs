﻿using IDanceSoft.Controlls;
using IDanceSoft.Views;
using System;
using Xamarin.Forms;

namespace IDanceSoft.Services
{
    public class MasterDetailPageNavigator
    {
        public void OpenPage(Type pageType)
        {
            var transitionNavigationPage = Application.Current.MainPage as TransitionNavigationPage;
            if (transitionNavigationPage != null)
            {
                var masterDetailPage = transitionNavigationPage.RootPage as MainPage;
                if (masterDetailPage != null)
                {
                    var page = (Page)Activator.CreateInstance(pageType);

					masterDetailPage.Detail = new NavigationPage(page)
                    {
                        BarBackgroundColor = Color.FromHex("#2573D8"),
                        BackgroundColor = Color.FromHex("#f6f6f6")
                    };
                    masterDetailPage.IsPresented = false;

                    masterDetailPage.GetMasterPage().ListView.SelectedItem = null;
                }
            }
        }
    }
}
