﻿using System;
using System.Linq;
using System.Threading.Tasks;
using IDanceSoft.API.Public.Model;
using IDanceSoft.Messages;
using IDanceSoft.Repository.Public.Model;
using Xamarin.Forms;

namespace IDanceSoft.Services
{
    public class GetClientListForCourseServiceHandler : ServiceHandler<ClientByCourseResult>
    {
        private readonly string _courseId;

        public GetClientListForCourseServiceHandler(string courseId)
        {
            _courseId = courseId;
        }

        protected override ClientByCourseResult GetRestServiceResult(User user)
        {
            var task = Task.Run(async () => await RestService.GetClientByCourse(_courseId, user.Login, user.Partner, user.SessionId));
            var result = task.Result;

            return result;
        }

        protected override void SendErrorMessage(int errorCode, string errorMessage)
        {
            var message = new GetClientListForCourseMessage { ErrorCode = errorCode, Message = errorMessage };
            Device.BeginInvokeOnMainThread(() =>
            {
                MessagingCenter.Send(message, GetClientListForCourseMessage.Tag);
            });
        }

        protected override void SendResultMessage(ClientByCourseResult result, int errorCode, string errorMessage)
        {
            int index = 1;
            var message = new GetClientListForCourseMessage { ErrorCode = errorCode, Message = errorMessage };
            message.Clients = result.Clients.Select(x => new Models.ClientListItem 
            {
                Id = x.Id,
                Index = index++,
                Email = x.Mail,
                Name = x.Name,
                Phone = x.Phone,
                Remarks = x.Message
            }).ToList();

            Device.BeginInvokeOnMainThread(() =>
            {
                MessagingCenter.Send(message, GetClientListForCourseMessage.Tag);
            });
        }
    }
}
