﻿using System.Threading.Tasks;
using IDanceSoft.API.Public.Model;
using IDanceSoft.Mapper;
using IDanceSoft.Messages;
using IDanceSoft.Repository.Public.Model;
using Xamarin.Forms;

namespace IDanceSoft.Services
{
    public class LoginServiceHandler : ServiceHandler<UserResult>
    {
        private readonly string _username;
        private readonly string _password;

        public LoginServiceHandler(string username, string password)
        {
            _username = username;
            _password = password;
        }

        public override void Handle()
        {
            var user = new User { Login = _username, Password = _password };
            var result = GetRestServiceResult(user);

            PrepareError(result, out int errorCode, out string errorMessage);

            if (errorCode == 0)
            {
                user = UserMapper.Map(result);
                user.Password = _password;
                var saveUserTask = Task.Run(async () => await UserRepository.Save(user));
            }

            SendResultMessage(result, errorCode, errorMessage);
        }

        protected override UserResult GetRestServiceResult(User user)
        {
            var task = Task.Run(async () => await RestService.Login(user.Login, user.Password));
            var result = task.Result;

            return result;
        }

        protected override void SendErrorMessage(int errorCode, string errorMessage)
        {
            SendResultMessage(null, errorCode, errorMessage);
        }

        protected override void SendResultMessage(UserResult result, int errorCode, string errorMessage)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                MessagingCenter.Send(new LoginMessage
                {
                    ErrorCode = errorCode,
                    Message = errorMessage
                }, LoginMessage.Tag);
            });
        }
    }
}
