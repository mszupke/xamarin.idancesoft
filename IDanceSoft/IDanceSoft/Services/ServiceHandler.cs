﻿using System.Threading.Tasks;
using IDanceSoft.API;
using IDanceSoft.API.Public;
using IDanceSoft.API.Public.Model;
using IDanceSoft.Repository;
using IDanceSoft.Repository.Public;
using IDanceSoft.Repository.Public.Model;

namespace IDanceSoft.Services
{
    public abstract class ServiceHandler<T> where T : BaseResult, new()
    {
        private const int NotLoggedInErrorCode = 1010;

        protected IRestService RestService { get; private set; }
        protected IUserRepository UserRepository { get; private set; }

        protected ServiceHandler()
        {
            RestService = new RestService();
            UserRepository = new UserRepository();
        }

        public virtual void Handle()
        {
            var user = GetUser();
            var result = GetRestServiceResult(user);

            PrepareError(result, out int errorCode, out string errorMessage);

            if (errorCode == NotLoggedInErrorCode)
            {
                SilentLogin(user, out int loginErrorCode, out string loginErrorMessage);

                if (loginErrorCode != 0)
                {
                    SendErrorMessage(loginErrorCode, loginErrorMessage);
                    return;
                }

                result = GetRestServiceResult(user);

                PrepareError(result, out errorCode, out errorMessage);

                if (errorCode != 0)
                {
                    SendErrorMessage(errorCode, errorMessage);
                    return;
                }
            }
            else if (errorCode != 0)
            {
                SendErrorMessage(errorCode, errorMessage);
                return;
            }

            SendResultMessage(result, errorCode, errorMessage);
        }

        protected abstract T GetRestServiceResult(User user);

        protected abstract void SendResultMessage(T result, int errorCode, string errorMessage);

        protected abstract void SendErrorMessage(int errorCode, string errorMessage);

        protected User GetUser()
        {
            var userTask = Task.Run(async () => await UserRepository.GetUser());
            var user = userTask.Result;

            return user;
        }

        protected void PrepareError(BaseResult result, out int errorCode, out string errorMessage)
        {
            errorMessage = result.Message;
            if (result.Error == null)
            {
                errorCode = 0;
            }
            else
            {
                int.TryParse(result.Error, out errorCode);
            }
        }

        protected void SilentLogin(User user, out int errorCode, out string errorMessage)
        {
            var task = Task.Run(async () => await RestService.Login(user.Login, user.Password));
            var userResult = task.Result;

            PrepareError(userResult, out errorCode, out errorMessage);

            if (errorCode == 0)
            {
                user.SessionId = userResult.SessionId;

                var saveUserTask = Task.Run(async () => await UserRepository.Save(user));
                var saveUserResult = saveUserTask.Result;
            }
        }
    }
}
