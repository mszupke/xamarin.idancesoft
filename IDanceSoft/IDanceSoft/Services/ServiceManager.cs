﻿using System.Threading.Tasks;
using IDanceSoft.Messages;
using IDanceSoft.Repository;
using IDanceSoft.Repository.Public;
using IDanceSoft.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(ServiceManager))]
namespace IDanceSoft.Services
{
    public class ServiceManager : IServiceManager
    {
        private readonly IUserRepository _userRepository;

        public ServiceManager()
        {
            _userRepository = new UserRepository();
        }

        public void HandleReadUserFromDatabaseTask()
        {
            var userTask = Task.Run(async () => await _userRepository.GetUser());
            var user = userTask.Result;

            var isUserLoggedIn = user != null;

            var message = new ReadUserFromDatabaseTaskMessage { IsUserExist = isUserLoggedIn };
            Device.BeginInvokeOnMainThread(() =>
            {
                MessagingCenter.Send(message, ReadUserFromDatabaseTaskMessage.Tag);
            });
        }

        public void HandleLogin(string username, string password)
        {
            var serviceHandler = new LoginServiceHandler(username, password);
            serviceHandler.Handle();
        }

        public void HandleSearchClient(string searchText)
        {
            var serviceHandler = new SearchClientServiceHandler(searchText);
            serviceHandler.Handle();
        }

        public void HandleGetClientById(string id)
        {
            var serviceHandler = new GetClientByIdServiceHandler(id);
            serviceHandler.Handle();
        }

        public void HandleSaveClient(string id, string name, string nameApp, string phone, string email, string cardNumber, string birthDate, string sex, string course, string partner, string confirm)
        {
            var message = new SaveClientMessage { ErrorCode = 0 };
            Device.BeginInvokeOnMainThread(() =>
            {
                MessagingCenter.Send(message, SaveClientMessage.Tag);
            });
        }

        public void HandleGetCourseList(string partner)
        {
            var serviceHandler = new GetCourseListServiceHandler();
            serviceHandler.Handle();
        }

        public void HandleGetClientListForCourse(string courseId)
        {
            var serviceHandler = new GetClientListForCourseServiceHandler(courseId);
            serviceHandler.Handle();
        }

        public void HandleLogoutFromDevice() 
        {
            var userTask = Task.Run(async () => await _userRepository.GetUser());
            var user = userTask.Result;

            var task = Task.Run(async () => await _userRepository.Delete(user));
            var userResult = task.Result;

            var message = new LogoutFromDeviceMessage();
            Device.BeginInvokeOnMainThread(() =>
            {
                MessagingCenter.Send(message, LogoutFromDeviceMessage.Tag);
            });
        }
    }
}
