﻿using System.Threading.Tasks;
using IDanceSoft.API.Public.Model;
using IDanceSoft.Messages;
using IDanceSoft.Models;
using IDanceSoft.Repository.Public.Model;
using Xamarin.Forms;

namespace IDanceSoft.Services
{
    public class GetClientByIdServiceHandler : ServiceHandler<ClientResult>
    {
        private readonly string _clientId;

        public GetClientByIdServiceHandler(string clientId)
        {
            _clientId = clientId;
        }

        protected override ClientResult GetRestServiceResult(User user)
        {
            var task = Task.Run(async () => await RestService.GetDataClient(_clientId, user.Login, user.Partner, user.SessionId));
            var result = task.Result;

            return result;
        }

        protected override void SendErrorMessage(int errorCode, string errorMessage)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                MessagingCenter.Send(new GetClientByIdMessage
                {
                    ErrorCode = errorCode,
                    Message = errorMessage
                }, GetClientByIdMessage.Tag);
            });
        }

        protected override void SendResultMessage(ClientResult result, int errorCode, string errorMessage)
        {
            var client = new Client
            {
                Id = result.Id,
                Name = result.Name,
                Connect = result.Connect,
                Code = null,
                Show = result.Show,
                Language = result.Language,
                RegisterDate = result.RegisterDate,
                Mail = result.Mail,
                Phone = result.Phone,
                Remarks = result.Remarks
            };

            Device.BeginInvokeOnMainThread(() =>
            {
                MessagingCenter.Send(new GetClientByIdMessage
                {
                    Client = client,
                    ErrorCode = errorCode,
                    Message = errorMessage
                }, GetClientByIdMessage.Tag);
            });
        }
    }
}
