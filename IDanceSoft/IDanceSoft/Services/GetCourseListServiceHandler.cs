﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IDanceSoft.API.Public.Model;
using IDanceSoft.Messages;
using IDanceSoft.Models;
using IDanceSoft.Repository.Public.Model;
using Xamarin.Forms;

namespace IDanceSoft.Services
{
    public class GetCourseListServiceHandler : ServiceHandler<CourseResult>
    {
        protected override CourseResult GetRestServiceResult(User user)
        {
            var task = Task.Run(async () => await RestService.GetCourseList(user.Login, user.Partner, user.SessionId));
            var result = task.Result;

            return result;
        }

        protected override void SendErrorMessage(int errorCode, string errorMessage)
        {
            var message = new GetCourseListMessage
            {
                ErrorCode = errorCode,
                Message = errorMessage,
                Courses = new List<Course>()
            };

            Device.BeginInvokeOnMainThread(() =>
            {
                MessagingCenter.Send(message, GetCourseListMessage.Tag);
            });
        }

        protected override void SendResultMessage(CourseResult result, int errorCode, string errorMessage)
        {
            var message = new GetCourseListMessage
            {
                ErrorCode = errorCode,
                Message = errorMessage,
                Courses = result.Courses.Select(x => new Course 
                { 
                    Id = x.Id, 
                    Name = x.Name, 
                    Day = int.Parse(x.CourseDay),
                    ClientId = x.ClientId,
                    CountGroupTime = x.CountGroupTime,
                    CreateDate = x.CreateDate,
                    GroupId = x.GroupId,
                    Hour = x.CourseHour,
                    Instructor = x.Instructor,
                    InstructorName = x.InstructorName,
                    LocalId = x.LocalId,
                    PublicDate = x.PublicDate,
                    PublicName = x.PublicName,
                    Remarks = x.Remarks,
                    RoomId = x.RoomId,
                    Status = x.CourseStatus
                }).ToList()
            };

            Device.BeginInvokeOnMainThread(() =>
            {
                MessagingCenter.Send(message, GetCourseListMessage.Tag);
            });
        }
    }
}
