﻿using System;
using System.Windows.Input;

namespace IDanceSoft.Commands
{
    public class RelayCommand : ICommand
    {
        private Action<object> _action;
        private Func<bool> _func;

        public RelayCommand(Action<object> action, Func<bool> func)
        {
            _action = action;
            _func = func;
        }

        public RelayCommand(Action<object> action) : this(action, null)
        {

        }

        public bool CanExecute(object parameter)
        {
            if (_func != null)
            {
                return _func();
            }

            return true;
        }

        public void Execute(object parameter)
        {
            if (parameter == null)
            {
                throw new InvalidOperationException("Command parameter is not set");
            }

            _action(parameter);
        }

        public event EventHandler CanExecuteChanged;
    }
}
