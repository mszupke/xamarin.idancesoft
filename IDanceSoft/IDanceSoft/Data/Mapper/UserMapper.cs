﻿using IDanceSoft.API.Public.Model;
using IDanceSoft.Models.DAO;

namespace IDanceSoft.Data.Mapper
{
    public class UserMapper
    {
        public static UserDao Map(UserResult userResult)
        {
            if (userResult == null)
            {
                return null;
            }

            UserDao userDao = new UserDao();
            userDao.SessionId = userResult.SessionId;
            userDao.Mail = userResult.UserData.Mail;
            userDao.Name = userResult.UserData.Name;
            userDao.Plec = userResult.UserData.Sex;
            userDao.Foto = userResult.UserData.Photo;
            userDao.SchoolId = userResult.SchoolData.Id;
            userDao.SchoolName = userResult.SchoolData.Name;
            userDao.SchoolOsoba = userResult.SchoolData.ContactName;

            return userDao;
        }
    }
}
