﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IDanceSoft.Data
{
    public abstract class GenericRepository<T> : IGenericRepository<T> where T : class, new()
    {
        protected SQLiteAsyncConnection Database { get; private set; }

        public GenericRepository()
        {
            Database = App.DatabaseManager.Database;
        }

        public AsyncTableQuery<T> AsQueryable() => Database.Table<T>();

        public async Task<List<T>> Get() => await Database.Table<T>().ToListAsync();

        public async Task<List<T>> Get<TValue>(Expression<Func<T, bool>> predicate = null, Expression<Func<T, TValue>> orderBy = null)
        {
            var query = Database.Table<T>();

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            if (orderBy != null)
            {
                query = query.OrderBy<TValue>(orderBy);
            }

            return await query.ToListAsync();
        }

        public async Task<T> Get(int id) => await Database.FindAsync<T>(id);

        public async Task<T> Get(Expression<Func<T, bool>> predicate) => await Database.FindAsync<T>(predicate);

        public async Task<int> Insert(T entity) => await Database.InsertAsync(entity);

        public async Task<int> Update(T entity) => await Database.UpdateAsync(entity);

        public async Task<int> Delete(T entity) => await Database.DeleteAsync(entity);
    }
}
