﻿using IDanceSoft.Data;
using IDanceSoft.Models.DAO;
using Xamarin.Forms;

[assembly: Dependency(typeof(UserRepository))]
namespace IDanceSoft.Data
{
    internal class UserRepository : GenericRepository<UserDao>, IUserRepository
    {
        public bool IsUserLoggedIn()
        {
            var all = Get();
            return all.Result.Count != 0;
        }
    }
}
