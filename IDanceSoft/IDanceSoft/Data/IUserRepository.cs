﻿using IDanceSoft.Models.DAO;

namespace IDanceSoft.Data
{
    internal interface IUserRepository : IGenericRepository<UserDao>
    {
        bool IsUserLoggedIn();
    }
}
