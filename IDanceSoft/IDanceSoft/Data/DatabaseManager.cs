﻿using IDanceSoft.Models.DAO;
using SQLite;

namespace IDanceSoft.Data
{
    public class DatabaseManager
    {
        public SQLiteAsyncConnection Database { get; private set; }

        public DatabaseManager(string dbPath)
        {
            Database = new SQLiteAsyncConnection(dbPath);

            Database.CreateTableAsync<UserDao>().Wait();
        }
    }
}
