﻿using IDanceSoft.Controlls;
using IDanceSoft.Messages;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IDanceSoft.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SplashPage : ContentPage
	{
		public SplashPage ()
		{
			InitializeComponent ();

            HandleReceivedMessages();
        }

        private void HandleReceivedMessages()
        {
            MessagingCenter.Subscribe<ReadUserFromDatabaseTaskMessage>(this, ReadUserFromDatabaseTaskMessage.Tag, message =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Debug.WriteLine("StartReadUserFromDatabaseTaskMessage: " + message.IsUserExist);
                    // UserDialogs.Instance.HideLoading();

                    if (message.IsUserExist)
                    {
                        var transitionMessage = new OpenMainPageMessage { TransitionType = TransitionType.Fade };
                        MessagingCenter.Send(transitionMessage, OpenMainPageMessage.Tag);
                    }
                    else
                    {
                        var transitionMessage = new OpenLoginPageMessage { TransitionType = TransitionType.Fade };
                        MessagingCenter.Send(transitionMessage, OpenLoginPageMessage.Tag);
                    }
                });
            });

            MessagingCenter.Subscribe<CancelledMessage>(this, CancelledMessage.Tag, message => 
            {
                Device.BeginInvokeOnMainThread(() => 
                {
                    Debug.WriteLine("StartReadUserFromDatabaseTaskMessage Cancelled");
                    // UserDialogs.Instance.HideLoading();
                });
            });

            MessagingCenter.Subscribe<OpenLoginPageMessage>(this, OpenLoginPageMessage.Tag, message =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    var transitionType = message.TransitionType;
                    PushToPage(new LoginPage(), transitionType);
                });
            });

            MessagingCenter.Subscribe<OpenMainPageMessage>(this, OpenMainPageMessage.Tag, message =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    var transitionType = message.TransitionType;
                    PushToPage(new MainPage(), transitionType);
                });
            });
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            Device.BeginInvokeOnMainThread(() =>
            {
                // UserDialogs.Instance.ShowLoading("Loading");

                var message = new StartReadUserFromDatabaseTaskMessage();
                MessagingCenter.Send(message, StartReadUserFromDatabaseTaskMessage.Tag);
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            MessagingCenter.Unsubscribe<ReadUserFromDatabaseTaskMessage>(this, ReadUserFromDatabaseTaskMessage.Tag);
            MessagingCenter.Unsubscribe<CancelledMessage>(this, CancelledMessage.Tag);
            MessagingCenter.Unsubscribe<OpenLoginPageMessage>(this, OpenLoginPageMessage.Tag);
            MessagingCenter.Unsubscribe<OpenMainPageMessage>(this, OpenMainPageMessage.Tag);
        }

        private void PushToPage(Page page, TransitionType transitionType)
        {
            var transitionNavigationPage = Parent as TransitionNavigationPage;

            if (transitionNavigationPage != null)
            {
                transitionNavigationPage.TransitionType = transitionType;
                Navigation.PushAsync(page);

                Navigation.RemovePage(this);
            }
        }
    }
}