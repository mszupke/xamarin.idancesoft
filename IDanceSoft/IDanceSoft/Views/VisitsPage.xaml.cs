﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IDanceSoft.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class VisitsPage : ContentPage
	{
		public VisitsPage ()
		{
			InitializeComponent ();
            Title = "Wizyty";
		}
    }
}