﻿using IDanceSoft.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IDanceSoft.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPageMaster : ContentPage
    {
        public ListView ListView;

        public MainPageMaster()
        {
            InitializeComponent();

			BindingContext = new MainPageMasterViewModel(Navigation);
            ListView = MenuItemsListView;
        }
    }
}