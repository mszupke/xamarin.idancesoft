﻿using System;
using System.Collections.Generic;
using IDanceSoft.Models;
using IDanceSoft.ViewModels;
using Xamarin.Forms;

namespace IDanceSoft.Views
{
    public partial class ClientEditPage : ContentPage
    {
		public ClientEditPage(ClientListItem client)
        {
            InitializeComponent();
			Title = "Edytuj dane klienta";

			var viewModel = new ClientEditViewModel(Navigation, client.Id);
			BindingContext = viewModel;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            ((BaseViewModel)BindingContext).OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            ((BaseViewModel)BindingContext).OnDisappearing();
        }
    }
}
