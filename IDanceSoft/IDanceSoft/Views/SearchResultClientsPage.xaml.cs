﻿using System;
using System.Collections.Generic;
using IDanceSoft.ViewModels;
using Xamarin.Forms;

namespace IDanceSoft.Views
{
	public partial class SearchResultClientsPage : ContentPage
	{
		public SearchResultClientsPage(string searchText)
		{
			InitializeComponent();
			Title = "Wyniki wyszukiwania";

			var viewModel = new SearchResultClientsViewModel(Navigation);
			viewModel.SearchText = searchText;

			BindingContext = viewModel;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			((BaseViewModel)BindingContext).OnAppearing();
		}
	}
}
