﻿using IDanceSoft.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IDanceSoft.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPageDetail : ContentPage
    {
        public MainPageDetail()
        {
            InitializeComponent();
            Title = "Studio Tańca Rytm";

            var viewModel = new MainPageDetailViewModel(Navigation);
            BindingContext = viewModel;
        }
    }
}