﻿using IDanceSoft.Controlls;
using IDanceSoft.ViewModels;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IDanceSoft.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ClientsPage : ContentPage
	{
		public ClientsPage ()
		{
			InitializeComponent ();
            Title = "Klienci";

            // var vehicleTypes = new List<string>() { "Klienci", "Obecności" };
            // segment.Children = vehicleTypes;

			BindingContext = new ClientsViewModel(Navigation);
		}

		void Handle_SelectedItemChanged(object sender, SelectedItemChangedEventArgs e)
        {
            // ItemSelectedText.Text = $"{e.SelectedItem}";
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            ((BaseViewModel)BindingContext).OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            ((BaseViewModel)BindingContext).OnDisappearing();
        }
    }
}