﻿using IDanceSoft.Messages;
using IDanceSoft.Models;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IDanceSoft.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : MasterDetailPage
    {
        public MainPage()
        {
            InitializeComponent();
            MasterPage.ListView.ItemSelected += ListView_ItemSelected;

            //HandleReceivedMessages();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            NavigationPage.SetHasNavigationBar(this, false);
        }

        //private void HandleReceivedMessages()
        //{
        //    MessagingCenter.Subscribe<LoginMessage>(this, LoginMessage.Tag, message =>
        //    {
        //        Device.BeginInvokeOnMainThread(() =>
        //        {
        //            if (message.ErrorCode == 0)
        //            {

        //            }
        //        });
        //    });
        //}

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MainPageMenuItem;
            if (item == null)
                return;

            var page = (Page)Activator.CreateInstance(item.TargetType);
            page.Title = item.Title;

			Detail = new NavigationPage(page)
			{
				BarBackgroundColor = Color.FromHex("#2573D8"),
				BackgroundColor = Color.FromHex("#f6f6f6")
			};

            IsPresented = false;

            MasterPage.ListView.SelectedItem = null;
        }

        public MainPageMaster GetMasterPage()
        {
            return MasterPage;
        }
    }
}