﻿using System;
using System.Windows.Input;
using Acr.UserDialogs;
using IDanceSoft.Commands;
using IDanceSoft.Messages;
using IDanceSoft.Models;
using Xamarin.Forms;

namespace IDanceSoft.ViewModels
{
	public class ClientEditViewModel : ClientViewModel
    {
		public ICommand SaveClientCommand { get; set; }

		public ClientEditViewModel(INavigation navigation, string id) 
            : base(navigation, id)
        {
            SaveClientCommand = new RelayCommand(new Action<object>(HandleSaveClient));
        }

		public override void OnAppearing()
		{
            base.OnAppearing();

            MessagingCenter.Subscribe<GetClientByIdMessage>(this, GetClientByIdMessage.Tag, message =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    if (message.ErrorCode == 0)
                    {
                        Client = message.Client;
                        UserDialogs.Instance.HideLoading();
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        UserDialogs.Instance.Alert(message.Message);
                    }
                });
            });

            MessagingCenter.Subscribe<SaveClientMessage>(this, SaveClientMessage.Tag, message =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    if (message.ErrorCode == 0)
                    {
                        var config = new AlertConfig();
                        config.OnAction = () =>
                        {
                            Navigation.PopAsync(true);
                        };

                        config.Message = "Zapisano dane klienta";

                        UserDialogs.Instance.HideLoading();
                        UserDialogs.Instance.Alert(config);
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                    }
                });
            });

			Device.BeginInvokeOnMainThread(() =>
            {
                UserDialogs.Instance.ShowLoading("Loading");

				var message = new StartGetClientByIdMessage();
                message.Id = Id;

				MessagingCenter.Send(message, StartGetClientByIdMessage.Tag);
            });
		}

        public override void OnDisappearing()
        {
            base.OnDisappearing();

            MessagingCenter.Unsubscribe<GetClientByIdMessage>(this, GetClientByIdMessage.Tag);
            MessagingCenter.Unsubscribe<SaveClientMessage>(this, SaveClientMessage.Tag);
        }

		protected override void HandleSaveClient(object obj)
		{
			Device.BeginInvokeOnMainThread(() =>
			{
				UserDialogs.Instance.ShowLoading("Zapisywanie danych klienta");

				var message = new StartSaveClientMessage
				{
					Id = Client.Id,
					Name = Client.Name,
					Nameapp = Client.Connect,
					Email = Client.Mail,
					Phone = Client.Phone,
					BirthDate = Client.BirthDate,
					CardNumber = Client.Code,
					// Confirm = Client.Confirm,
					// Course = Client.Course,
					// Partner = Client.Partner,
					Sex = Client.Sex
				};

				MessagingCenter.Send(message, StartSaveClientMessage.Tag);
			});
		}
    }
}
