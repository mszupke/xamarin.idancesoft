﻿using Acr.UserDialogs;
using IDanceSoft.Commands;
using IDanceSoft.Messages;
using IDanceSoft.Models;
using IDanceSoft.Views;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace IDanceSoft.ViewModels
{
    public class ClientsViewModel : BaseViewModel
    {
        private IList<Course> _courses;
        public IList<Course> Courses
        {
            get { return _courses; }
            set
            {
                _courses = value;
                OnPropertyChanged("Courses");
            }
        }

        private Course _selectedCourse;
        public Course SelectedCourse
        {
            get { return _selectedCourse; }
            set
            {
                _selectedCourse = value;
                OnPropertyChanged("SelectedCourse");
                SetDaysInMonth();

                if (_selectedCourse != null)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        UserDialogs.Instance.ShowLoading("Pobieranie listy klientów");

                        var message = new StartGetClientListForCourseMessage();
                        message.CourseId = _selectedCourse.GroupId;

                        MessagingCenter.Send(message, StartGetClientListForCourseMessage.Tag);
                    });
                }
            }
        }

        private IList<ClientListItem> _clients;
        public IList<ClientListItem> Clients
        {
            get { return _clients; }
            set
            {
                _clients = value;
                OnPropertyChanged("Clients");
            }
        }

        private string _clientsButtonBackgroundColor;
        public string ClientsButtonBackgroundColor
        {
            get { return _clientsButtonBackgroundColor; }
            set
            {
                _clientsButtonBackgroundColor = value;
                OnPropertyChanged("ClientsButtonBackgroundColor");
            }
        }

        private string _clientsButtonTextColor;
        public string ClientsButtonTextColor
        {
            get { return _clientsButtonTextColor; }
            set
            {
                _clientsButtonTextColor = value;
                OnPropertyChanged("ClientsButtonTextColor");
            }
        }

        private string _visitsButtonBackgroundColor;
        public string VisitsButtonBackgroundColor
        {
            get { return _visitsButtonBackgroundColor; }
            set
            {
                _visitsButtonBackgroundColor = value;
                OnPropertyChanged("VisitsButtonBackgroundColor");
            }
        }

        private string _visitsButtonTextColor;
        public string VisitsButtonTextColor
        {
            get { return _visitsButtonTextColor; }
            set
            {
                _visitsButtonTextColor = value;
                OnPropertyChanged("VisitsButtonTextColor");
            }
        }

        private bool _isClientsPanelVisible;
        public bool IsClientsPanelVisible
        {
            get { return _isClientsPanelVisible; }
            set
            {
                _isClientsPanelVisible = value;
                OnPropertyChanged("IsClientsPanelVisible");
            }
        }

        private bool _isVisitsPanelVisible;
        public bool IsVisitsPanelVisible
        {
            get { return _isVisitsPanelVisible; }
            set
            {
                _isVisitsPanelVisible = value;
                OnPropertyChanged("IsVisitsPanelVisible");
            }
        }

        private string _date;
        public string Date
        {
            get { return _date; }
            set
            {
                _date = value;
                OnPropertyChanged("Date");
            }
        }

        private string _day1;
        public string Day1
        {
            get { return _day1; }
            set
            {
                _day1 = value;
                OnPropertyChanged("Day1");
            }
        }

        private string _day2;
        public string Day2
        {
            get { return _day2; }
            set
            {
                _day2 = value;
                OnPropertyChanged("Day2");
            }
        }

        private string _day3;
        public string Day3
        {
            get { return _day3; }
            set
            {
                _day3 = value;
                OnPropertyChanged("Day3");
            }
        }

        private string _day4;
        public string Day4
        {
            get { return _day4; }
            set
            {
                _day4 = value;
                OnPropertyChanged("Day4");
            }
        }

        private string _day5;
        public string Day5
        {
            get { return _day5; }
            set
            {
                _day5 = value;
                OnPropertyChanged("Day5");
            }
        }

        private int?[] _days;
        public int?[] Days
        {
            get { return _days; }
            set
            {
                _days = value;
                OnPropertyChanged("Days");
            }
        }

		public ICommand ShowClientsCommand { get; set; }
		public ICommand ShowVisitsCommand { get; set; }
		public ICommand SearchCommand { get; set; }
        public ICommand EditClientCommand { get; set; }

        public ClientsViewModel(INavigation navigation) : base(navigation)
        {
            Date = DateTime.Today.ToString("MM.yyyy");

            ShowClientsCommand = new RelayCommand(new Action<object>(HandleShowClients));
            ShowVisitsCommand = new RelayCommand(new Action<object>(HandleShowVisits));
            SearchCommand = new RelayCommand(new Action<object>(HandleSearch));
            EditClientCommand = new RelayCommand(new Action<object>(HandleEditCommand));

            HandleShowClients(null);
        }

		public override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<GetCourseListMessage>(this, GetCourseListMessage.Tag, message =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    if (message.ErrorCode == 0)
                    {
                        Courses = message.Courses;
                        UserDialogs.Instance.HideLoading();
                    }
                    else
                    {
                        Courses = new List<Course>();
                        UserDialogs.Instance.HideLoading();

                        HandleError(message);
                    }
                });
            });

            MessagingCenter.Subscribe<GetClientListForCourseMessage>(this, GetClientListForCourseMessage.Tag, message =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    if (message.ErrorCode == 0)
                    {
                        Clients = message.Clients;
                        UserDialogs.Instance.HideLoading();
                    }
                    else
                    {
                        Clients = new List<ClientListItem>();
                        UserDialogs.Instance.HideLoading();
                        UserDialogs.Instance.Alert(message.Message);
                    }
                });
            });

            Device.BeginInvokeOnMainThread(() =>
            {
                UserDialogs.Instance.ShowLoading("Ładowanie kursów");

                var message = new StartGetCourseListMessage();
                message.SchoolId = "";

                MessagingCenter.Send(message, StartGetCourseListMessage.Tag);
            });
        }

        public override void OnDisappearing()
        {
            base.OnDisappearing();

            MessagingCenter.Unsubscribe<GetCourseListMessage>(this, GetCourseListMessage.Tag);
            MessagingCenter.Unsubscribe<GetClientListForCourseMessage>(this, GetClientListForCourseMessage.Tag);
        }

		private void HandleSearch(object obj)
		{
			Navigation.PushAsync(new SearchResultClientsPage(obj.ToString()));
		}

        private void HandleEditCommand(object obj)
        {
            Navigation.PushAsync(new ClientEditPage(obj as ClientListItem));
        }

        private void HandleShowVisits(object obj)
		{
			if (IsVisitsPanelVisible) return;

			IsClientsPanelVisible = false;
			ClientsButtonBackgroundColor = "#00000000";
			ClientsButtonTextColor = "#2573D8";

			IsVisitsPanelVisible = true;
			VisitsButtonBackgroundColor = "#2573D8";
			VisitsButtonTextColor = "#ffffff";
		}

		private void HandleShowClients(object obj)
		{
			if (IsClientsPanelVisible) return;

			IsVisitsPanelVisible = false;
			VisitsButtonBackgroundColor = "#00000000";
            VisitsButtonTextColor = "#2573D8";

			IsClientsPanelVisible = true;
			ClientsButtonBackgroundColor = "#2573D8";
			ClientsButtonTextColor = "#ffffff";
		}

        private void SetDaysInMonth() 
        {
            if (SelectedCourse != null && !string.IsNullOrEmpty(Date)) 
            {
                var d = Date.Split('.');
                if (d.Length == 2) {
                    int month;
                    int year;

                    int.TryParse(d[0], out month);
                    int.TryParse(d[1], out year);

                    var daysInMonth = DateTime.DaysInMonth(year, month);

                    int?[] days = new int?[5];
                    int index = 0;
                    for (int i = 1; i <= daysInMonth; i++) 
                    {
                        var dt = new DateTime(year, month, i);
                        if ((int)dt.DayOfWeek == SelectedCourse.Day) 
                        {
                            days[index++] = i;
                        }
                    }

                    Day1 = days[0].HasValue ? days[0].ToString() : null;
                    Day2 = days[1].HasValue ? days[1].ToString() : null;
                    Day3 = days[2].HasValue ? days[2].ToString() : null;
                    Day4 = days[3].HasValue ? days[3].ToString() : null;
                    Day5 = days[4].HasValue ? days[4].ToString() : null;
                    Days = days;
                }
            }
        }
    }
}
