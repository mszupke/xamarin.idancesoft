﻿using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Acr.UserDialogs;
using IDanceSoft.Messages;
using IDanceSoft.Views;
using Xamarin.Forms;

namespace IDanceSoft.ViewModels
{
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

		protected INavigation Navigation { get; private set; }

		public BaseViewModel(INavigation navigation)
		{
			Navigation = navigation;
		}

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public virtual void OnAppearing() 
        {
            MessagingCenter.Subscribe<LogoutFromDeviceMessage>(this, LogoutFromDeviceMessage.Tag, message =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    UserDialogs.Instance.HideLoading();
                    Navigation.PushAsync(new LoginPage());

                    var existingPages = Navigation.NavigationStack.Where(x => x.GetType() != typeof(LoginPage)).ToList();
                    foreach (var page in existingPages)
                    {
                        Navigation.RemovePage(page);
                    }
                });
            });
        }

        public virtual void OnDisappearing() 
        {
            MessagingCenter.Unsubscribe<LogoutFromDeviceMessage>(this, LogoutFromDeviceMessage.Tag);
        }

        protected void HandleError(BaseMessage message) 
        {
            var alertConfig = new AlertConfig
            {
                Message = message.Message
            };

            switch (message.ErrorCode)
            {
                case 1010:
                    alertConfig.OnAction = () =>
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            UserDialogs.Instance.ShowLoading("Trwa wylogowywanie z urządzenia");

                            var logoutMessage = new StartLogoutFromDeviceMessage();
                            MessagingCenter.Send(logoutMessage, StartLogoutFromDeviceMessage.Tag);
                        });
                    };
                    break;
                default:
                    break;
            }

            UserDialogs.Instance.Alert(alertConfig);
        }
    }
}
