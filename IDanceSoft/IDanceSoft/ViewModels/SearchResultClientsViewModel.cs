﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using IDanceSoft.Commands;
using IDanceSoft.Messages;
using IDanceSoft.Models;
using IDanceSoft.Views;
using Xamarin.Forms;

namespace IDanceSoft.ViewModels
{
	public class SearchResultClientsViewModel : BaseViewModel
	{
		private string _searchText;
		public string SearchText
		{
			get { return _searchText; }
			set
			{
				_searchText = value;
				OnPropertyChanged("SearchText");
			}
		}

		private IList<ClientListItem> _clients;
		public IList<ClientListItem> Clients
		{
			get { return _clients; }
			set
			{
				_clients = value;
				OnPropertyChanged("Clients");
			}
		}

		public ICommand EditClientCommand { get; set; }

		public SearchResultClientsViewModel(INavigation navigation) : base(navigation)
		{
            Clients = new List<ClientListItem>();

            EditClientCommand = new RelayCommand(new Action<object>(HandleEditCommand));
		}

		public override void OnAppearing() 
		{
            base.OnAppearing();

            MessagingCenter.Subscribe<SearchClientMessage>(this, SearchClientMessage.Tag, message =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    if (message.ErrorCode == 0)
                    {
                        Clients = message.Clients;
                        UserDialogs.Instance.HideLoading();
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        UserDialogs.Instance.Alert(message.Message);
                    }
                });
            });

			Device.BeginInvokeOnMainThread(() =>
            {
                UserDialogs.Instance.ShowLoading("Loading");

				var message = new StartSearchClientMessage();
				message.SearchText = SearchText;

				MessagingCenter.Send(message, StartSearchClientMessage.Tag);
            });
		}

        public override void OnDisappearing()
        {
            base.OnDisappearing();

            MessagingCenter.Unsubscribe<SearchClientMessage>(this, SearchClientMessage.Tag);
        }

        private async void HandleEditCommand(object obj)
		{
			await Navigation.PushAsync(new ClientEditPage(obj as ClientListItem));
		}
	}
}
