﻿using System;
using System.Windows.Input;
using IDanceSoft.Commands;
using IDanceSoft.Models;
using Xamarin.Forms;

namespace IDanceSoft.ViewModels
{
	public abstract class ClientViewModel : BaseViewModel
    {
		private string _id;
		public string Id 
		{
			get { return _id; }
			set
			{
				_id = value;
				OnPropertyChanged("Id");
			}
		}

		private string _code;
        public string Code
		{
			get { return _code; }
			set
			{
				_code = value;
				OnPropertyChanged("Code");
			}
		}

		private string _mail;
        public string Mail
		{
			get { return _mail; }
			set
			{
				_mail = value;
				OnPropertyChanged("Mail");
			}
		}

		private string _phone;
        public string Phone
		{
			get { return _phone; }
			set
			{
				_phone = value;
				OnPropertyChanged("Phone");
			}
		}

		private string _name;
        public string Name
		{
			get { return _name; }
			set
			{
				_name = value;
				OnPropertyChanged("Name");
			}
		}

		private string _connect;
        public string Connect
		{
			get { return _connect; }
			set
			{
				_connect = value;
				OnPropertyChanged("Connect");
			}
		}

		private string _registerDate;
        public string RegisterDate
		{
			get { return _registerDate; }
			set
			{
				_registerDate = value;
				OnPropertyChanged("RegisterDate");
			}
		}

		private string _show;
        public string Show
		{
			get { return _show; }
			set
			{
				_show = value;
				OnPropertyChanged("Show");
			}
		}

		private string _language;
        public string Language
		{
			get { return _language; }
            set 
			{
				_language = value;
				OnPropertyChanged("Language");
			}
		}

		private Client _client;
		protected Client Client 
		{
			get { return _client; }
			set
			{
				_client = value;

				Id = _client?.Id;
                Code = _client?.Code;
                Mail = _client?.Mail;
                Phone = _client?.Phone;
                Name = _client?.Name;
                Connect = _client?.Connect;
                RegisterDate = _client?.RegisterDate;
                Show = _client?.Show;
                Language = _client?.Language;
			}
		}



		public ClientViewModel(INavigation navigation, string id) : base(navigation)
        {
            Id = id;
        }

		protected abstract void HandleSaveClient(object obj);
	}
}
