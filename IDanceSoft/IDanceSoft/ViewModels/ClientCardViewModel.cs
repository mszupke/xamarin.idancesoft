﻿using System;
using Xamarin.Forms;

namespace IDanceSoft.ViewModels
{
    public class ClientCardViewModel : ClientViewModel
    {
        public ClientCardViewModel(INavigation navigation, string id) 
            : base(navigation, id)
        {
        }

        public override void OnAppearing()
        {
            base.OnAppearing();
        }

        public override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        protected override void HandleSaveClient(object obj)
        {

        }
    }
}
