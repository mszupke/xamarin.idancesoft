﻿using IDanceSoft.Models;
using IDanceSoft.Views;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace IDanceSoft.ViewModels
{
    public class MainPageMasterViewModel : BaseViewModel
    {
        public ObservableCollection<MainPageMenuItem> MenuItems { get; set; }

		public MainPageMasterViewModel(INavigation navigation) : base(navigation)
        {
            MenuItems = new ObservableCollection<MainPageMenuItem>(new[]
            {
                new MainPageMenuItem { Id = 0, Title = "Wizyty", Image = "ic_action_visits.png", TargetType = typeof(VisitsPage) },
                new MainPageMenuItem { Id = 1, Title = "Klienci", Image = "ic_action_clients.png", TargetType = typeof(ClientsPage) },
                new MainPageMenuItem { Id = 2, Title = "Ustawienia", Image = "ic_action_settings.png", TargetType = typeof(SettingsPage) },
                new MainPageMenuItem { Id = 3, Title = "Pomoc", Image = "ic_action_help.png", TargetType = typeof(HelpPage) }
            });
        }

		public override void OnAppearing()
        {
            base.OnAppearing();
        }

        public override void OnDisappearing()
        {
            base.OnDisappearing();
        }
    }
}
