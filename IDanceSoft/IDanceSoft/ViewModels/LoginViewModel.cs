﻿using Acr.UserDialogs;
using IDanceSoft.API;
using IDanceSoft.API.Public;
using IDanceSoft.Commands;
using IDanceSoft.Messages;
using IDanceSoft.Views;
using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace IDanceSoft.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private IRestService _restService;

        private string _login;
        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                OnPropertyChanged("Login");
            }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                OnPropertyChanged("Password");
            }
        }

        public ICommand LoginCommand { get; set; }
        public ICommand RegisterCommand { get; set; }
        public ICommand HelpCommand { get; set; }

		public LoginViewModel(INavigation navigation) : base(navigation)
        {
            LoginCommand = new RelayCommand(new Action<object>(HandleLogin));
            RegisterCommand = new RelayCommand(new Action<object>(HandleRegister));
            HelpCommand = new RelayCommand(new Action<object>(HandleHelp));

            _restService = new RestService();
        }

		public override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<LoginMessage>(this, LoginMessage.Tag, message =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    if (message.ErrorCode == 0)
                    {
                        UserDialogs.Instance.HideLoading();
                        Navigation.PushAsync(new MainPage());
                        Navigation.RemovePage(Navigation.NavigationStack[Navigation.NavigationStack.Count - 2]);
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        UserDialogs.Instance.Alert(message.Message);
                    }
                });
            });
        }

        public override void OnDisappearing()
        {
            base.OnDisappearing();

            MessagingCenter.Unsubscribe<LoginMessage>(this, LoginMessage.Tag);
        }

        private bool CanHandleLogin()
        {
            return !string.IsNullOrEmpty(Login) && !string.IsNullOrEmpty(Password);
        }

        private void HandleLogin(object obj)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                UserDialogs.Instance.ShowLoading("Logowanie");

                var message = new StartLoginMessage
                {
                    Username = Login,
                    Password = Password
                };

                MessagingCenter.Send(message, StartLoginMessage.Tag);
            });
        }

        private void HandleRegister(object obj)
        {

        }

        private void HandleHelp(object obj)
        {

        }
    }
}
