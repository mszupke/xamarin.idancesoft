﻿using Xamarin.Forms;

namespace IDanceSoft.ViewModels
{
    public class SplashViewModel : BaseViewModel
    {
		public SplashViewModel(INavigation navigation) : base(navigation)
		{

		}

		public override void OnAppearing()
        {
            base.OnAppearing();
        }

        public override void OnDisappearing()
        {
            base.OnDisappearing();
        }
    }
}
