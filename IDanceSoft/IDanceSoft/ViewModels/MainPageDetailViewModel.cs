﻿using System;
using System.Windows.Input;
using IDanceSoft.Commands;
using IDanceSoft.Views;
using Xamarin.Forms;

namespace IDanceSoft.ViewModels
{
    public class MainPageDetailViewModel : BaseViewModel
    {
        public ICommand VisitsButtonCommand { get; set; }

        public ICommand ClientsButtonCommand { get; set; }

        public ICommand SettingsButtonCommand { get; set; }

        public ICommand HelpButtonCommand { get; set; }

        public MainPageDetailViewModel(INavigation navigation) : base(navigation)
        {
            VisitsButtonCommand = new RelayCommand(new Action<object>(HandleVisitsButtonCommand));
            ClientsButtonCommand = new RelayCommand(new Action<object>(HandleClientsButtonCommand));
            SettingsButtonCommand = new RelayCommand(new Action<object>(HandleSettingsButtonCommand));
            HelpButtonCommand = new RelayCommand(new Action<object>(HandleHelpButtonCommand));
        }

        private void HandleVisitsButtonCommand(object obj)
        {
            Navigation.PushAsync(new VisitsPage());
        }

        private void HandleClientsButtonCommand(object obj)
        {
            Navigation.PushAsync(new ClientsPage());
        }

        private void HandleSettingsButtonCommand(object obj)
        {
            Navigation.PushAsync(new SettingsPage());
        }

        private void HandleHelpButtonCommand(object obj)
        {
            Navigation.PushAsync(new HelpPage());
        }

        public override void OnAppearing()
        {
            base.OnAppearing();
        }

        public override void OnDisappearing()
        {
            base.OnDisappearing();
        }
    }
}
