﻿using System;
namespace IDanceSoft.Models
{
    public class Client
    {
		public string Id { get; set; }
		public string Code { get; set; }
		public string Mail { get; set; }
		public string Phone { get; set; }
		public string BirthDate { get; set; }
		public string Name { get; set; }
		public string Connect { get; set; }
		public string RegisterDate { get; set; }
		public string Show { get; set; }
		public string Language { get; set; }    
		public string Sex { get; set; }
        public string Remarks { get; set; }
    }
}
