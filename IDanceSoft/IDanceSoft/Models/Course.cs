﻿namespace IDanceSoft.Models
{
    public class Course
    {
        /// <summary>
        /// ID klienta
        /// </summary>
        /// <value>ID klienta</value>
        public string ClientId { get; set; }

        /// <summary>
        /// ID kursu w ofercie
        /// </summary>
        /// <value>ID kursu w ofercie</value>
        public string Id { get; set; }

        /// <summary>
        /// Dzień kursu
        /// </summary>
        /// <value>0 - 6</value>
        public int Day { get; set; }

        /// <summary>
        /// Godzina kursu
        /// </summary>
        /// <value>Np: 10:00 - 11:00</value>
        public string Hour { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <value>ID lokalizacji</value>
        public string LocalId { get; set; }

        /// <summary>
        /// Osoba odpowiedzialna
        /// </summary>
        /// <value>Instruktor kursu</value>
        public string Instructor { get; set; }

        /// <summary>
        /// Status kursu
        /// </summary>
        /// <value>
        /// 0 - kurs trwa, brak wolnych miejsc
        /// 1 - zapisy na kurs
        /// 11 - kurs trwa, można się zapisać
        /// 2 - kurs się zakończył i jest w archiwum
        /// 3 - wydarzenie publiczne
        /// </value>
        public string Status { get; set; }

        /// <summary>
        /// Uwagi
        /// </summary>
        /// <value>Uwagi</value>
        public string Remarks { get; set; }

        /// <summary>
        /// ID grupy
        /// </summary>
        /// <value>ID grupy</value>
        public string GroupId { get; set; }

        /// <summary>
        /// ID sali
        /// </summary>
        /// <value>ID sali</value>
        public string RoomId { get; set; }

        /// <summary>
        /// Data dodania
        /// </summary>
        /// <value>Data dodania</value>
        public string CreateDate { get; set; }

        /// <summary>
        /// Nazwa kursu
        /// </summary>
        /// <value>Nazwa kursu</value>
        public string Name { get; set; }

        /// <summary>
        /// Imię instruktora
        /// </summary>
        /// <value>Imię instruktora</value>
        public string InstructorName { get; set; }

        /// <summary>
        /// Ilość terminów grupy
        /// </summary>
        /// <value>Ilość terminów grupy</value>
        public string CountGroupTime { get; set; }

        /// <summary>
        /// Nazwa wydarzenia
        /// </summary>
        /// <value>Nazwa wydarzenia</value>
        public string PublicName { get; set; }

        /// <summary>
        /// Data wydarzenia
        /// </summary>
        /// <value>Data wydarzenia</value>
        public string PublicDate { get; set; }
    }
}
