﻿namespace IDanceSoft.Models.DAO
{
    public class UserDao : BaseDao
    {
        public string SessionId { get; set; }
        public string Mail { get; set; }
        public string Name { get; set; }
        public string Plec { get; set; }
        public string Foto { get; set; }

        public string SchoolId { get; set; }
        public string SchoolName { get; set; }
        public string SchoolOsoba { get; set; }
        public string SchoolFoto { get; set; }
        public string SchoolOpis { get; set; }
        public string SchoolLink { get; set; }
        public string SchoolNip { get; set; }
        public string SchoolRegon { get; set; }
        public string SchoolAdres { get; set; }
        public string SchoolMiasto { get; set; }
        public string SchoolKod { get; set; }
        public string SchoolKonto { get; set; }
        public string SchoolProcentRef { get; set; }
        public string SchoolProcentIns { get; set; }
        public string SchoolNrUmowy { get; set; }
        public string SchoolShowHome { get; set; }
        public string SchoolMail { get; set; }
        public string SchoolPhone { get; set; }
        public string SchoolAdmin { get; set; }
        public string SchoolDotpay { get; set; }
        public string SchoolPaypal { get; set; }
        public string SchoolPaymentType { get; set; }
        public string SchoolCurrency { get; set; }
        public string SchoolLang { get; set; }
        public string SchoolSmsName { get; set; }
        public string SchoolMailHost { get; set; }
        public string SchoolMailUser { get; set; }
        public string SchoolMailPassword { get; set; }
        public string SchoolMailTls { get; set; }
        public string SchoolMailLimit { get; set; }
        public string SchoolMailSleep { get; set; }
        public string SchoolCardList { get; set; }
        public string SchoolCountry { get; set; }
        public string SchoolTimezone1 { get; set; }
        public string SchoolLinkReturn { get; set; }
        public string SchoolBackground { get; set; }
        public string SchoolDesc { get; set; }
        public string SchoolTypeC { get; set; }
    }
}
