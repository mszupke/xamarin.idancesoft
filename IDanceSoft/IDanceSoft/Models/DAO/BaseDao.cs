﻿using SQLite;

namespace IDanceSoft.Models.DAO
{
    public class BaseDao
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public BaseDao()
        {

        }
    }
}
