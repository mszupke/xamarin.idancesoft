﻿namespace IDanceSoft.Models
{
    public class ClientListItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int Index { get; set; }
        public string Remarks { get; set; }
    }
}
