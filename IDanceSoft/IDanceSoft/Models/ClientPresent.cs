﻿using System;
namespace IDanceSoft.Models
{
    public class ClientPresent
    {
        public string Id { get; set; }
        public string Date { get; set; }
        public string ClientId { get; set; }
        public string CourseId { get; set; }
        public string Control { get; set; }
    }
}
