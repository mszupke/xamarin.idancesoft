﻿using System;
using IDanceSoft.API.Public.Model;
using IDanceSoft.Repository.Public.Model;

namespace IDanceSoft.Mapper
{
    public class UserMapper
    {
        public static User Map(UserResult userResult) 
        {
            User user = new User();

            user.Partner = userResult.UserData.Id;
            user.Login = userResult.Login;
            user.SessionId = userResult.SessionId;
            user.Photo = userResult.SchoolData.Photo;

            return user;
        }
    }
}
