﻿namespace IDanceSoft
{
    public class Constants
    {
		public const string Id = "Id";
		public const string Name = "Name";
		public const string NameApp = "NameApp";
		public const string Phone = "Phone";
		public const string Email = "Email";
		public const string CardNumber = "CardNumber";
		public const string BirthDate = "BirthDate";
		public const string Sex = "Sex";
		public const string Course = "Course";
		public const string Partner = "Partner";
		public const string Confirm = "Confirm";

        public const string SchoolId = "SchoolId";
        public const string CourseId = "CourseId";

        public const string Username = "Username";
        public const string Password = "Password";
    }
}
