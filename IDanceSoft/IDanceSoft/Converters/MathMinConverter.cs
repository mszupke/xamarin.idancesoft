﻿using IDanceSoft.Proxy;
using System;
using System.Globalization;

namespace IDanceSoft.Converters
{
    public class MathMinConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            double min = double.MaxValue;

            for (int i = 0; i < values.Length; i++)
            {
                min = Math.Min(min, (double)values[i]);
            }

            return min < 0 ? 0 : min;
        }
    }
}
