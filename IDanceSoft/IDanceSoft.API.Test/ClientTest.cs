﻿using System.Linq;
using System.Threading.Tasks;
using IDanceSoft.API.Public;
using IDanceSoft.API.Public.Model;
using NUnit.Framework;

namespace IDanceSoft.API.Test
{
    [TestFixture]
    public class ClientTest
    {
        private const string login = "clear@dancebook.pl";
        private const string password = "123";

        private IRestService _restService;
        private UserResult userResult;

        [SetUp]
        public void Init() 
        {
            _restService = new RestService();

            var loginTask = Task.Run<UserResult>(async () => await _restService.Login(login, password));
            userResult = loginTask.Result;
        }

        [Test]
        public void GetClientTest() 
        {
            var task = Task.Run<ClientResult>(async () => await _restService.GetDataClient(userResult.UserData.Id, userResult.Login, userResult.SchoolData.Id, userResult.SessionId));
            var clientResult = task.Result;
            Assert.AreEqual(null, clientResult.Error);
        }

        [Test]
        public void SearchClientTest() 
        {
            var task = Task.Run<SearchClientResult>(async () => await _restService.SearchClient("a", userResult.Login, userResult.SchoolData.Id, userResult.SessionId));
            var result = task.Result;
            // Assert.AreEqual(null, result.Error);

            var client = result.Clients.FirstOrDefault();
            if (client != null) 
            {
                var task2 = Task.Run<ClientResult>(async () => await _restService.GetDataClient(client.Id, userResult.Login, userResult.SchoolData.Id, userResult.SessionId));
                var clientResult = task2.Result;
            }
        }

        [TearDown]
        public void Clenup() 
        {
            var logoutTask = Task.Run<BaseResult>(async () => await _restService.Logout(login, userResult.SessionId));
            var logoutResult = logoutTask.Result;
        }
    }
}
