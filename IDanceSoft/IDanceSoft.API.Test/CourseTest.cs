﻿using System;
using System.Threading.Tasks;
using IDanceSoft.API.Public;
using IDanceSoft.API.Public.Model;
using NUnit.Framework;

namespace IDanceSoft.API.Test
{
    [TestFixture]
    public class CourseTest
    {
        private const string login = "clear@dancebook.pl";
        private const string password = "123";

        private IRestService _restService;
        private UserResult userResult;

        [SetUp]
        public void Init()
        {
            _restService = new RestService();

            var loginTask = Task.Run<UserResult>(async () => await _restService.Login(login, password));
            userResult = loginTask.Result;
        }

        [Test]
        public void GetCoursesTest() 
        {
            var task = Task.Run<CourseResult>(async () => await _restService.GetCourseList(userResult.Login, userResult.SchoolData.Id, userResult.SessionId));
            var clientResult = task.Result;
            Assert.AreEqual(null, clientResult.Error);
        }

        [TearDown]
        public void Clenup()
        {
            var logoutTask = Task.Run<BaseResult>(async () => await _restService.Logout(login, userResult.SessionId));
            var logoutResult = logoutTask.Result;
        }
    }
}
