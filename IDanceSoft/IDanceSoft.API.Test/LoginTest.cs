﻿using System.Threading.Tasks;
using IDanceSoft.API.Public;
using IDanceSoft.API.Public.Model;
using NUnit.Framework;

namespace IDanceSoft.API.Test
{
    [TestFixture]
    public class LoginTest
    {
        private IRestService _restService;

        [SetUp]
        public void Init() 
        {
            _restService = new RestService();
        }

        [Test]
        public void LoginSuccessful() 
        {
            var task = Task.Run<UserResult>(async () => await _restService.Login("clear@dancebook.pl", "123"));
            var a = task.Result;
            Assert.AreEqual(null, a.Error);

            var logoutTask = Task.Run<BaseResult>(async () => await _restService.Logout("clear@dancebook.pl", a.SessionId));
            var logoutResult = logoutTask.Result;
            Assert.AreEqual(null, logoutResult.Error);
        }

        [Test]
        public void LoginUnsuccessful() 
        {
            var task = Task.Run<UserResult>(async () => await _restService.Login("clear@dancebook.pla", "1234"));

            var a = task.Result;
            Assert.AreEqual("1", a.Error);
        }
    }
}
