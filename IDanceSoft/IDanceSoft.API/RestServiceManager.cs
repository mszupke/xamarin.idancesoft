﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace IDanceSoft.API
{
    internal class RestServiceManager<T>
    {
        private const string RestUrl = "https://idancesoft.com/klient/api.php";
        private const string ApiToken = "gJGJKGhF8mFJ6iJFE6ijHkggjjF";

        private readonly HttpClient client;

        public Dictionary<string, string> Parameters { get; private set; }

        public RestServiceManager(Dictionary<string, string> parameters)
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            client.Timeout = TimeSpan.FromSeconds(180);

            Parameters = parameters;
        }

        public async Task<T> GetTask() 
        {
            var uri = CreateUri();
            Debug.WriteLine(uri.AbsoluteUri);

            Stopwatch stopwatch = new Stopwatch();

            try
            {
                stopwatch.Start();

                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<T>(content);

                    return result;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"              ERROR {0}", ex.Message);
                throw;
            }
            finally
            {
                stopwatch.Stop();
                Debug.WriteLine(@"Login time: " + stopwatch.ElapsedMilliseconds + " miliseconds");
            }

            return default(T);
        }

        private Uri CreateUri()
        {
            var stringBuilder = new StringBuilder(RestUrl + "?");
            foreach (var parameter in Parameters)
            {
                stringBuilder.Append(parameter.Key + "=" + parameter.Value + "&");
            }

            return new Uri(stringBuilder.ToString());
        }
    }
}
