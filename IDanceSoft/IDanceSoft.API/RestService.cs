﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using IDanceSoft.API;
using IDanceSoft.API.Public;
using IDanceSoft.API.Public.Model;
using Xamarin.Forms;

[assembly: Dependency(typeof(RestService))]
namespace IDanceSoft.API
{
    public class RestService : IRestService
    {
        private const string ApiToken = "gJGJKGhF8mFJ6iJFE6ijHkggjjF";

        #region Login

        public Task<UserResult> Login(string login, string password)
        {
            var parameters = CreateParameters();
            parameters.Add("login", login);
            parameters.Add("haslo", password);

            var manager = new RestServiceManager<UserResult>(parameters);
            return manager.GetTask();
        }

        public Task<BaseResult> Logout(string login, string sessionId)
        {
            var parameters = CreateParameters();
            parameters.Add("logout", login);
            parameters.Add("sessi", sessionId);

            var manager = new RestServiceManager<BaseResult>(parameters);
            return manager.GetTask();
        }

        #endregion

        #region School

        public Task<RegisterSchoolResult> RegisterSchool(string regmail, string rname, string rpassword)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Client

        public Task<ClientResult> GetDataClient(string id, string login, string partner, string sessionId)
        {
            var parameters = CreateParameters();
            parameters.Add("module", "clients");
            parameters.Add("get_data_client", id);
            parameters.Add("partner", partner);
            parameters.Add("login", login);
            parameters.Add("sessi", sessionId);

            var manager = new RestServiceManager<ClientResult>(parameters);
            return manager.GetTask();
        }

        public Task<ClientByCourseResult> GetClientByCourse(string courseId, string login, string partner, string sessionId)
        {
            var parameters = CreateParameters();
            parameters.Add("module", "clients");
            parameters.Add("get_clients_of_course", courseId);
            parameters.Add("partner", partner);
            parameters.Add("login", login);
            parameters.Add("sessi", sessionId);

            var manager = new RestServiceManager<ClientByCourseResult>(parameters);
            return manager.GetTask();
        }

        public Task<SearchClientResult> SearchClient(string text, string login, string partner, string sessionId)
        {
            var parameters = CreateParameters();
            parameters.Add("module", "clients");
            parameters.Add("search_data_client", text);
            parameters.Add("partner", partner);
            parameters.Add("login", login);
            parameters.Add("sessi", sessionId);

            var manager = new RestServiceManager<SearchClientResult>(parameters);
            return manager.GetTask();
        }

        public Task<SaveClientResult> SaveClient(string id, string name, string nameapp, string phone, string email, string cardNumber, string birthDate, string sex, string courseId, string partner, string confirm)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Course

        public Task<CourseResult> GetCourseList(string login, string partner, string sessionId)
        {
            var parameters = CreateParameters();
            parameters.Add("module", "courses");
            parameters.Add("show_all_courses", "1");
            parameters.Add("partner", partner);
            parameters.Add("login", login);
            parameters.Add("sessi", sessionId);

            var manager = new RestServiceManager<CourseResult>(parameters);
            return manager.GetTask();
        }

        #endregion

        #region Payment

        public Task<PaymentResult> GetClientPayments(string showAllPayments, string partner, string clientId, string month, string year)
        {
            throw new NotImplementedException();
        }

        public Task<PaymentResult> GetCoursePayments(string showAllPayments, string partner, string courseId, string month, string year)
        {
            throw new NotImplementedException();
        }

        public Task<PaymentResult> GetPaymentsWithinDate(string showAllPayments, string partner, string courseId, string localId, string startDate, string endDate)
        {
            throw new NotImplementedException();
        }

        public Task<SavePaymentResult> SavePayment(string paymentId, string partner, string courseId, string clientId, string title, string paymentValue, string subscriptionVisitCount, string expireMonths, string expireDays, string endDate, string proof, string amount)
        {
            throw new NotImplementedException();
        }

        public Task<DeletePaymentResult> DeletePayment(string paymentId, string partner)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Presents

        public Task<PresentResult> GetClientPresents(string showAllPresents, string clientId, string year, string month)
        {
            throw new NotImplementedException();
        }

        public Task<PresentResult> GetCoursePresents(string showAllPresents, string courseId, string yesr, string month)
        {
            throw new NotImplementedException();
        }

        public Task<SetPresentResult> SetPresent(string presentDate, string clientId, string courseId, string action, string control)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Locals

        public Task<LocalResult> GetLocalList(string location, string partner, string login)
        {
            throw new NotImplementedException();
        }

        #endregion

        private Dictionary<string, string> CreateParameters() 
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add("apitoken", ApiToken);

            return parameters;
        }
    }
}
