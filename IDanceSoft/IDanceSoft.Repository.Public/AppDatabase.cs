﻿using System;
using System.IO;
using IDanceSoft.Repository.Public.Model;
using SQLite;

namespace IDanceSoft.Repository.Public
{
    public sealed class AppDatabase
    {
        private static AppDatabase instance = null;
        private static readonly object padlock = new object();

        public SQLiteAsyncConnection Database { get; private set; }

        private AppDatabase() 
		{
            var dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "idancesoft.db3");

            Database = new SQLiteAsyncConnection(dbPath);
            Database.CreateTableAsync<User>().Wait();
        }

		public static AppDatabase Instance 
		{
			get 
			{
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new AppDatabase();
                    }
                    return instance;
                }
            }
		}
    }   
}
