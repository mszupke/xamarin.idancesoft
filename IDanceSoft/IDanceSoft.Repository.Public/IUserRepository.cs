﻿using System.Threading.Tasks;
using IDanceSoft.Repository.Public.Model;

namespace IDanceSoft.Repository.Public
{
    public interface IUserRepository : IBaseRepository<User>
    {
        Task<User> GetUser();
    }
}
