﻿using SQLite;

namespace IDanceSoft.Repository.Public.Model
{
    public class Entity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public Entity() 
        {

        }
    }
}
