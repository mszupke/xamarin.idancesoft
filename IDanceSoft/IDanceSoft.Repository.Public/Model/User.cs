﻿using IDanceSoft.Utils.Security;

namespace IDanceSoft.Repository.Public.Model
{
    public class User : Entity
    {
        private const string _passPhrase = "IDS123";

        private string _password;

        public string Partner { get; set; }

        public string SessionId { get; set; }

        public string Login { get; set; }

        public string Photo { get; set; }

        public string Password
        {
            get
            {
                var result = StringCipher.Decrypt(_password, _passPhrase);
                return result;
            }
            set
            {
                _password = StringCipher.Encrypt(value, _passPhrase);
            }
        }
    }
}
