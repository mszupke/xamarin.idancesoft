﻿using System.Threading.Tasks;
using IDanceSoft.Repository.Public.Model;

namespace IDanceSoft.Repository.Public
{
    public interface IBaseRepository<T> where T : Entity, new()
    {
        Task<T> Get(int id);

        Task<int> Save(T entity);

        Task<int> Delete(T entity);
    }
}
