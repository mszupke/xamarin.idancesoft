﻿using System.Threading.Tasks;
using IDanceSoft.API.Public.Model;

namespace IDanceSoft.API.Public
{
    public interface IRestService
    {
        Task<UserResult> Login(string login, string password);

        Task<BaseResult> Logout(string login, string sessionId);

        Task<RegisterSchoolResult> RegisterSchool(string regmail, string rname, string rpassword);

        Task<ClientResult> GetDataClient(string id, string login, string partner, string sessionId);

        Task<SaveClientResult> SaveClient(string id, string name, string nameapp, string phone, string email, string cardNumber, string birthDate, string sex, string courseId, string partner, string confirm);

        Task<SearchClientResult> SearchClient(string text, string login, string partner, string sessionId);

        Task<ClientByCourseResult> GetClientByCourse(string courseId, string login, string partner, string sessionId);

        Task<PaymentResult> GetClientPayments(string showAllPayments, string partner, string clientId, string month, string year);

        Task<PaymentResult> GetCoursePayments(string showAllPayments, string partner, string courseId, string month, string year);

        Task<PaymentResult> GetPaymentsWithinDate(string showAllPayments, string partner, string courseId, string localId, string startDate, string endDate);

        Task<SavePaymentResult> SavePayment(string paymentId, string partner, string courseId, string clientId, string title, string paymentValue, string subscriptionVisitCount, string expireMonths, string expireDays, string endDate, string proof, string amount);

        Task<DeletePaymentResult> DeletePayment(string paymentId, string partner);

        Task<PresentResult> GetClientPresents(string showAllPresents, string clientId, string year, string month);

        /// <summary>
        /// Gets the course presents.
        /// </summary>
        /// <returns>The course presents.</returns>
        /// <param name="showAllPresents">Show all presents.</param>
        /// <param name="courseId">Course identifier.</param>
        /// <param name="yesr">Yesr.</param>
        /// <param name="month">Month.</param>
        Task<PresentResult> GetCoursePresents(string showAllPresents, string courseId, string yesr, string month);

        /// <summary>
        /// Zaznaczenie / odznaczenie obecności
        /// </summary>
        /// <returns>The present.</returns>
        /// <param name="presentDate">Data obecnności</param>
        /// <param name="clientId">ID klienta</param>
        /// <param name="courseId">ID kursu</param>
        /// <param name="action">Akcja: 1 - znaznacz obecność, 0 - skasuj obecność</param>
        /// <param name="control">wiersz kontrolny - godzina kursu</param>
        Task<SetPresentResult> SetPresent(string presentDate, string clientId, string courseId, string action, string control);

        /// <summary>
        /// Lokalizacje i sale
        /// </summary>
        /// <returns>The local list.</returns>
        /// <param name="location">Flaga wejścia do trybu</param>
        /// <param name="partner">ID szkoły</param>
        /// <param name="login">Login użytkownika - potrzebny do sprawdzenia praw do lokalizacji</param>
        Task<LocalResult> GetLocalList(string location, string partner, string login);

        /// <summary>
        /// Gets the course list by client.
        /// </summary>
        /// <returns>The course list by client.</returns>
        /// <param name="login">Login.</param>
        /// <param name="partner">Partner.</param>
        /// <param name="sessionId">Session identifier.</param>
        Task<CourseResult> GetCourseList(string login, string partner, string sessionId);

        //Task<CourseResult> GetCourseListByClient(string login, string partner, string sessionId);
    }
}
