﻿using Newtonsoft.Json;

namespace IDanceSoft.API.Public.Model
{
    public class BaseResult
    {
        [JsonProperty(PropertyName = "MESSAGE")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "ERROR")]
        public string Error { get; set; }

        public bool HasErrors()
        {
            return !string.IsNullOrEmpty(Error);
        }
    }
}
