﻿using Newtonsoft.Json;

namespace IDanceSoft.Service.Public.Model
{
    public class UserDataResult : BaseResult
    {
        [JsonProperty(PropertyName = "mail")]
        public string Mail { get; set; }

        [JsonProperty(PropertyName = "login")]
        public string Login { get; set; }

        [JsonProperty(PropertyName = "data")]
        public string RegisterDate { get; set; }

        [JsonProperty(PropertyName = "prawa")]
        public int Rights { get; set; }

        [JsonProperty(PropertyName = "pokazuj")]
        public int Show { get; set; }

        [JsonProperty(PropertyName = "ban")]
        public int Ban { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "plec")]
        public string Sex { get; set; }

        [JsonProperty(PropertyName = "foto")]
        public string Photo { get; set; }
    }
}
