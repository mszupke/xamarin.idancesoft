﻿using System;
using Newtonsoft.Json;

namespace IDanceSoft.API.Public.Model
{
    public class SearchClientItemResult
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "mail")]
        public string Mail { get; set; }

        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "connect")]
        public string Connect { get; set; }
    }
}
