﻿using Newtonsoft.Json;

namespace IDanceSoft.API.Public.Model
{
    public class CourseItemResult
    {
        [JsonProperty(PropertyName = "Id")]
        public string ClientId { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "godzinaformat")]
        public string CourseHour { get; set; }

        [JsonProperty(PropertyName = "dzien")]
        public string CourseDay { get; set; }

        [JsonProperty(PropertyName = "lokal")]
        public string LocalId { get; set; }

        [JsonProperty(PropertyName = "instruktor")]
        public string Instructor { get; set; }

        [JsonProperty(PropertyName = "zapisy")]
        public string CourseStatus { get; set; }

        [JsonProperty(PropertyName = "uwagi")]
        public string Remarks { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string GroupId { get; set; }

        [JsonProperty(PropertyName = "sala")]
        public string RoomId { get; set; }

        [JsonProperty(PropertyName = "date")]
        public string CreateDate { get; set; }

        [JsonProperty(PropertyName = "kurs_name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "ins_name")]
        public string InstructorName { get; set; }

        [JsonProperty(PropertyName = "count_g")]
        public string CountGroupTime { get; set; }

        [JsonProperty(PropertyName = "public_name")]
        public string PublicName { get; set; }

        [JsonProperty(PropertyName = "public_data")]
        public string PublicDate { get; set; }
    }
}
