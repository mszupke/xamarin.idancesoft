﻿using Newtonsoft.Json;

namespace IDanceSoft.API.Public.Model
{
    public class UserResult : BaseResult
    {
        [JsonProperty(PropertyName = "SESSION_ID")]
        public string SessionId { get; set; }

        [JsonProperty(PropertyName = "LOGIN")]
        public string Login { get; set; }

        [JsonProperty(PropertyName = "DATA")]
        public UserDataResult UserData { get; set; }

        [JsonProperty(PropertyName = "SCHOOL")]
        public UserSchoolResult SchoolData { get; set; }
    }
}
