﻿using System;
using Newtonsoft.Json;

namespace IDanceSoft.API.Public.Model
{
    public class ClientResult : BaseResult
    {
        [JsonProperty(PropertyName = "Id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "nr")]
        public string Code { get; set; }

        [JsonProperty(PropertyName = "mail")]
        public string Mail { get; set; }

        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "connect")]
        public string Connect { get; set; }

        [JsonProperty(PropertyName = "data")]
        public string RegisterDate { get; set; }

        [JsonProperty(PropertyName = "show")]
        public string Show { get; set; }

        [JsonProperty(PropertyName = "lang")]
        public string Language { get; set; }

        [JsonProperty(PropertyName = "desc")]
        public string Remarks { get; set; }
    }
}
