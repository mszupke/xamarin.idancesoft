﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace IDanceSoft.API.Public.Model
{
    public class ClientByCourseResult : BaseResult
    {
        [JsonProperty(PropertyName = "count")]
        public int Count { get; set; }

        [JsonProperty(PropertyName = "clients")]
        public List<ClientResult> Clients { get; set; }
    }
}
