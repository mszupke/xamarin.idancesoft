﻿using Newtonsoft.Json;

namespace IDanceSoft.API.Public.Model
{
    public class PresentItemResult
    {
        [JsonProperty(PropertyName = "data")]
        public string PresentDate { get; set; }

        [JsonProperty(PropertyName = "id_client")]
        public string ClientId { get; set; }

        [JsonProperty(PropertyName = "id_course")]
        public string CourseId { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "control")]
        public string Control { get; set; }
    }
}
