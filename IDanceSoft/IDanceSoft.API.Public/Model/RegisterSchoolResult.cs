﻿using System;
using Newtonsoft.Json;

namespace IDanceSoft.API.Public.Model
{
    public class RegisterSchoolResult : BaseResult
    {
        [JsonProperty(PropertyName = "partner")]
        public string Partner { get; set; }
    }
}
