﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace IDanceSoft.API.Public.Model
{
    public class SearchClientResult : BaseModuleResult
    {
        [JsonProperty(PropertyName = "count")]
        public int Count { get; set; }

        [JsonProperty(PropertyName = "DATA")]
        public List<SearchClientItemResult> Clients { get; set; }
    }
}
