﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace IDanceSoft.API.Public.Model
{
    public class PaymentResult : BaseModuleResult
    {
        [JsonProperty(PropertyName = "count")]
        public int Count { get; set; }

        [JsonProperty(PropertyName = "payments")]
        public List<PaymentItemResult> Payments { get; set; }
    }
}
