﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace IDanceSoft.API.Public.Model
{
    public class CourseResult : BaseResult
    {
        [JsonProperty(PropertyName = "amount_courses")]
        public int CountCourses { get; set; }

        [JsonProperty(PropertyName = "amount_event")]
        public int CountEvents { get; set; }

        [JsonProperty(PropertyName = "amount_lessons")]
        public int CountLessons { get; set; }

        [JsonProperty(PropertyName = "courses")]
        public List<CourseItemResult> Courses { get; set; }

        [JsonProperty(PropertyName = "events")]
        public List<CourseItemResult> Events { get; set; }

        [JsonProperty(PropertyName = "lessons")]
        public List<CourseItemResult> Lessons { get; set; }
    }
}
