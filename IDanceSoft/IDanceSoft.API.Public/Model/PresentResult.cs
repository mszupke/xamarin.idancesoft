﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace IDanceSoft.API.Public.Model
{
    public class PresentResult : BaseModuleResult
    {
        [JsonProperty(PropertyName = "count")]
        public int Count { get; set; }

        [JsonProperty(PropertyName = "presents")]
        public List<PresentItemResult> Presents { get; set; }
    }
}
