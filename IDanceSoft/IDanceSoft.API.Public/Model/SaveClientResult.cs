﻿using System;
using Newtonsoft.Json;

namespace IDanceSoft.API.Public.Model
{
    public class SaveClientResult : BaseResult
    {
        [JsonProperty(PropertyName = "id_client")]
        public string Id { get; set; }
    }
}
