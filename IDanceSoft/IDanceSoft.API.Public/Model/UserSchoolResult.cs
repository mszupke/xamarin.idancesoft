﻿using Newtonsoft.Json;

namespace IDanceSoft.API.Public.Model
{
    public class UserSchoolResult : BaseResult
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "osoba")]
        public string ContactName { get; set; }

        [JsonProperty(PropertyName = "foto")]
        public string Photo { get; set; }

        [JsonProperty(PropertyName = "opis")]
        public string InvoiceCompanyName { get; set; }

        [JsonProperty(PropertyName = "link")]
        public string Keyword { get; set; }

        [JsonProperty(PropertyName = "nip")]
        public string NIP { get; set; }

        [JsonProperty(PropertyName = "regon")]
        public string Regon { get; set; }

        [JsonProperty(PropertyName = "adres")]
        public string Address { get; set; }

        [JsonProperty(PropertyName = "miasto")]
        public string City { get; set; }

        [JsonProperty(PropertyName = "kod")]
        public string PostalCode { get; set; }

        [JsonProperty(PropertyName = "konto")]
        public string BankAccount { get; set; }

        [JsonProperty(PropertyName = "procent_ref")]
        public string CommissionForRecommendation { get; set; }

        [JsonProperty(PropertyName = "procent_ins")]
        public string CommissionForFranchise { get; set; }

        [JsonProperty(PropertyName = "nr_umowy")]
        public string AgreementNumber { get; set; }

        [JsonProperty(PropertyName = "show_home")]
        public string ShowHome { get; set; }

        [JsonProperty(PropertyName = "mail")]
        public string Mail { get; set; }

        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }

        [JsonProperty(PropertyName = "admin")]
        public string AdministratorMail { get; set; }

        [JsonProperty(PropertyName = "dotpay")]
        public string DotpayId { get; set; }

        [JsonProperty(PropertyName = "paypal")]
        public string PaypalId { get; set; }

        [JsonProperty(PropertyName = "payment_type")]
        public string PaymentType { get; set; }

        [JsonProperty(PropertyName = "currency")]
        public string Currency { get; set; }

        [JsonProperty(PropertyName = "lang")]
        public string Language { get; set; }

        [JsonProperty(PropertyName = "sms_name")]
        public string SMSName { get; set; }

        [JsonProperty(PropertyName = "mail_host")]
        public string MailHost { get; set; }

        [JsonProperty(PropertyName = "mail_user")]
        public string MailUser { get; set; }

        [JsonProperty(PropertyName = "mail_password")]
        public string MailPassword { get; set; }

        [JsonProperty(PropertyName = "mail_tls")]
        public string MailTls { get; set; }

        [JsonProperty(PropertyName = "mail_limit")]
        public string MailLimit { get; set; }

        [JsonProperty(PropertyName = "mail_sleep")]
        public string MailSleep { get; set; }
    }
}
